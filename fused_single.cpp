/*

for execution "no planning" with critical sections

BACKWARDS = the main loop in opposite direction
GENERATOR = generates a plan
typ&3: 0 =all rows
       1 = skip head and tail
       2 = skip all empty 
typ<4: initial start position = tid 
else:  initial start position = tid*n/th            
*/

#ifdef BACKWARDS
  void fused_singleb(float *x, float *rest y, float *rest y2, uint n,int typ)
#else 
#ifdef GENERATOR
     void fused_single_gen(float *x, float *rest y, float *rest y2, uint n,int typ)
  #else
     void fused_single(float *x, float *rest y, float *rest y2, uint n,int typ)  
#endif
#endif
{
    int i,i2;
  uint l;
  uint p_reg[MAX_BLO];
  uint p_th[MAX_BLO];
  uint indexy[MAX_THR];
  volatile uint executed=0;
  uint crc=0;
  double start,start2,end,end2;
  uint podil=(n)/threads;//(n+threads-1)/threads;
  char mutexx[MAX_BLO];
  char mutexy[MAX_BLO];

          start=omp_get_wtime();

          //std::cout << " CSR " << end-start << std::endl;

   for(l=0;l<no_blocks;l++) {   
   pole_exec[l].done=0;
   //mx[i]=my[i]=0;
   }  
  //std::cout << " bx= " << noboundsx << std::endl; 
  //std::cout << " by= " << noboundsy << std::endl;  
  for(l=0;l<noboundsx;l++) mutexx[l]=0;
  for(l=0;l<noboundsy;l++) mutexy[l]=0;
  int podil2=no_blocks/threads; 
  #pragma omp parallel num_threads(threads) private(end2,i,i2) shared(start,start2,executed,pole_exec)         
  {

    uint tid,j,kx,ky,index;

    tid=omp_get_thread_num();
    memset(y+tid*podil,0,podil*4);
    memset(y2+tid*podil,0,podil*4);
    #pragma omp barrier
     //for(uint i=0;i<n;i++) y[i]=0.0;
    //memset(y,0,n*4);
    #pragma omp master
       start2=omp_get_wtime();
    //
    do{
      /*#pragma omp flush(executed) 
      #pragma omp atomic
        crc++;
      std::cout << " tid=" << tid << " crc=" << crc << std::endl;    */
      #pragma omp critical 
      {
        /*std::cout << "ENTER th= " << tid <<std::endl; 
           fflush(stdout);  */    
      for(i2=0;((i2<no_blocks)&&(executed<no_blocks));i2++)
      {
        //std::cout << " tid0=" << tid << " i2=" << i2 << " executed=" << executed <<std::endl; 
        #ifdef BACKWARDS
        if (typ>=4)
          i=(no_blocks+i2-tid*podil2)%no_blocks;
        else
          i=(no_blocks+i2-tid)%no_blocks; 
        #else
        if (typ>=4)
          i=(i2+tid*podil2)%no_blocks;
        else
          i=(i2+tid)%no_blocks;  
        #endif  
          
          

        //omp_flush(pole_exec)
        
        if (pole_exec[i].done>0) 
        {
          //#pragma omp flush(executed)
          continue;    
        }
        //std::cout << " tid0a=" << tid << " i=" << i <<std::endl;
        /*std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        fflush(stdout);  */
        //if (pole_pol[i].pocty>(1<<16))
        //do{
        ky=0;
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          if (mutexy[j]==0) ky++;
          else break;
        if (ky!=(pole_pol[i].my2-pole_pol[i].my1)) 
        {  
          /*std::cout << " tid= " << tid;
          std::cout << " i2= " << i << std::endl;
          fflush(stdout);            
          */                                                                                                       
          //for(j=pole_pol[i].my1;j<(pole_pol[i].my1+ky);j++)
          //  omp_unset_lock(mutexy+j);
          /*std::cout << " tid= " << tid;
          std::cout << " i3= " << i << std::endl;
          fflush(stdout);   */   
          //#pragma omp flush(executed)
        /*#pragma omp master
        {
           std::cout << "Skip1: " << i << " / " << executed << " / " << no_blocks <<std::endl; 
           fflush(stdout);
        } */   
          //i+=tid;             
          continue;  
        }   
        kx=0;
        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++)
          if (mutexx[j]==0) kx++;
          else break;
        if (kx!=(pole_pol[i].mx2-pole_pol[i].mx1)) 
        {  
          /*std::cout << " tid= " << tid;
          std::cout << " i2= " << i << std::endl;
          fflush(stdout);            
          */
          //for(j=pole_pol[i].mx1;j<(pole_pol[i].mx1+kx);j++)
           // omp_unset_lock(mutexx+j);
            
        //for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          //omp_unset_lock(mutexy+j);  
          /*std::cout << " tid= " << tid;
          std::cout << " i3= " << i << std::endl;
          fflush(stdout);   */  
          //#pragma omp flush(executed)  
        /*#pragma omp master
        {
           std::cout << "Skip2: " << i << " / " << executed << " / " << no_blocks <<std::endl; 
           fflush(stdout);
        } */       
         // i+=tid;       
          continue;  
        }   
        break;
        } //end of for = vybran region
        /*
        std::cout << "th= " << tid << "   i=" << i <<std::endl; 
           fflush(stdout);     */
        if ((i2<no_blocks)&&(executed<no_blocks))
        { // opravdu vybran
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          mutexy[j]=1;

        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++)
          mutexx[j]=1;        
        // region choosen
          #ifdef GENERATOR
              index=executed++; 
            p_th[index]=tid;
            p_reg[index]=i;
          #else
            executed++; 
          #endif
       //#pragma omp flush(executed)            
         //std::cout << " tid1=" << tid << " i2=" << i2 << " i=" << i<< std::endl;
        pole_exec[i].done=1;
        }
        } //end of kr. sekce
        //#pragma omp flush(pole_exec)   
        
        if ((i2>=no_blocks)||(executed>=no_blocks)) continue;
        
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif

          switch(typ&3){
          case 0: fused_csr_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, x+pole_pol[i].starty);
          break;
          case 1: fused_csr_f16AA2(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, pole_pol[i].lower, pole_pol[i].upper, x+pole_pol[i].starty);
          break;
          case 2: fused_csr_f16AA3(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, pole_pol[i].lower, pole_pol[i].upper, x+pole_pol[i].starty);
          break;
          }  
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif

          fused_coo_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty, x+pole_pol[i].starty); 
          //fused_coo_f16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }
        /* std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */
        //std::cout << " tid2=" << tid << " i2=" << i2 << std::endl;
        #pragma omp critical  
        {
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          mutexy[j]=0;

        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++)
          mutexx[j]=0;
        }
 
       //std::cout << " tid3=" << tid << " i2=" << i2 << std::endl;   

        
        /*#pragma omp master
        {
           std::cout << "Exec: " << i << " / "  << executed << " / " << no_blocks <<std::endl; 
           fflush(stdout);
        } */
  /*std::cout << "Exec: " << i << " / "  << executed << " / " << no_blocks <<std::endl; 
           fflush(stdout);     */ 
  //#pragma omp flush(executed)      
  }while((executed<no_blocks)); //end of do   //&&(crc<10000));
          #ifdef TIMER3
          end2=omp_get_wtime();
          std::cout << " FUSSED dyn " << tid << " = "<< end2-start2 << std::endl;
          //std::cout  << tid << std::endl;        
          #endif         
          //std::cout << " tid4=" << tid << " ex=" << executed << std::endl;
      }  // end of par.
  #ifdef GENERATOR    
 for(i=0;i<threads;i++)
 {
   plan_thr_num[i]=0;
 }
 for(i=0;i<no_blocks;i++)  
 {
   //std::cout << " i="<< i << " t=" << p_th[i] << " r=" << p_reg[i] << std::endl; 
   plan_thr_num[p_th[i]]++;
  }
  uint temp;
  l=0;
  for(i=0;i<threads;i++)
  {
    temp=plan_thr_num[i];
    indexy[i]=plan_thr_num[i]=l;
    l+=temp;
  }
  plan_thr_num[threads]=no_blocks;
   
  for(i=0;i<no_blocks;i++)
  {
     plan_thr_reg[indexy[p_th[i]]++]=p_reg[i]; 
     //std::cout << pole_reg[i][k2] << " "; 
   }
   /*
  for(i=0;i<=threads;i++)
  {     
     std::cout << i << " = " << plan_thr_num[i] << std::endl;
   }   
   
  for(i=0;i<no_blocks;i++)
  {
     std::cout << i << " = " << plan_thr_reg[i] << std::endl; 
   }   
   //std::cout <<std::endl; 
  */   
 #endif
      
 #ifdef TIMER2
 end=omp_get_wtime();
 std::cout << " FUSSED " << end-start << std::endl;               
 //if (crc>=1000)       
 std::cout << " FUSSED exec" << executed << " crc=" << crc << std::endl; 
 #endif
/*
 if (crc>=10000)
 std::cout << " FUSSED exec" << executed << " crc=" << crc << std::endl;
 
#ifdef BACKWARDS
  std::cout << " FUSSED " << end-start << std::endl;
#elseif
  #ifdef GENERATOR
     std::cout << " FUSSED " << end-start << std::endl;
  #elseif
     std::cout << " FUSSED " << end-start << std::endl; 
  #endif
#endif  */
 return;
}