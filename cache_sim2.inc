//#include"p4cel128.def"
//#define DETAIL_PRINT 1

/*
CACHE EMULATOR to measure the number of cache misses 
*/
#define MAX_CACHE_B1 64l
#define MAX_CACHE_H1 128l
#define MAX_CACHE_S1 8l

#define MAX_CACHE_B2 64l
#define MAX_CACHE_H2 1024l
#define MAX_CACHE_S2 8l

#define MAX_CACHE_B3 0l
#define MAX_CACHE_H3 0l
#define MAX_CACHE_S3 0l

#define ulong uint64_t
#define t_counter ulong *

ulong CACHE_B1;
ulong CACHE_H1;
ulong CACHE_S1;

ulong CACHE_B2;
ulong CACHE_H2;
ulong CACHE_S2;

ulong CACHE_B3;
ulong CACHE_H3;
ulong CACHE_S3;

t_counter read_ops;
t_counter cache1_miss;
t_counter cache2_miss;
t_counter cache3_miss;
t_counter nulovy;

struct cache_line {
  ulong addr, rank;
};

struct cache_line *cache_l1, *cache_l2, *cache_l3;

void *alokace(ulong a, ulong s)
/*
  allocate a items with size s
*/
{
  ulong *b;
  b = (ulong *)calloc(a, s);
  if (b == NULL) {
    printf("\n Neni volne misto v pameti.\n");
    abort();
  }
  return b;
}

void nuluj_counter(t_counter x) {
  /*
    set the counter x to 0
  */
  x[0] = 0l;
  x[1] = 0l;
}

t_counter alok_counter() {
  /*
    allocate a counter
  */
  return (t_counter)malloc(2 * sizeof(ulong));
}

void zvys_counter(t_counter x, ulong i) {
  /*
    incerase a vlaue of the counter x by i
  */
  ulong lo, hi;

  lo = x[0];
  hi = x[1];
  lo += i;
  while (lo >= 1000000l) {
    lo -= 1000000l;
    hi++;
  }
  x[0] = lo;
  x[1] = hi;
}

double ret_counter(t_counter x) {
  /*
    returns value of counter x as double
  */
  ulong lo, hi;
  double a;
  lo = x[0];
  hi = x[1];
  a = ((double)lo);
  a += ((double)hi) * 1e6;
  return a;
}

double ret_miss1() { return ret_counter(cache1_miss); }

double ret_miss2() { return ret_counter(cache2_miss); }

double ret_miss3() { return ret_counter(cache3_miss); }

void tisk_cislo_3(int x) {
  printf("%u", x);
  // printf(" m3a ");
}

void tisk_cislo_3a(int x) {
  int a;

  a = x / 100;
  x = x % 100;
  printf("%u", a);
  // printf(" m3b ");
  a = x / 10;
  x = x % 10;
  printf("%u", a);
  // printf(" m3b ");
  printf("%u", x);
  // printf(" m3b ");
}

void tisk_cislo_6(ulong x) {
  ulong lo, hi;

  lo = x % 1000l;
  hi = x / 1000l;

  if (hi != 0l) {
    tisk_cislo_3(hi);
    // printf(" m2a ");
    tisk_cislo_3a(lo);
  } else {
    tisk_cislo_3(lo);
  }
}

void tisk_cislo_6a(ulong x) {
  ulong lo, hi;

  lo = x % 1000l;
  hi = x / 1000l;

  tisk_cislo_3a(hi);
  // printf(" m2b ");
  tisk_cislo_3a(lo);
}

void tisk_counter(t_counter x) {
  /*
    prints a vlaue of the counter x
  */
  ulong lo, hi;

  lo = x[0];
  hi = x[1];
#ifdef DETAIL_PRINT
  printf("%lu g  %lu m  %lu k %lu\n", hi / 1000l, hi % 1000l, lo / 1000l,
         lo % 1000l);
#endif
#ifndef DETAIL_PRINT
  if (hi != 0l) {
    tisk_cislo_6(hi);
    // printf(" m1a ");
    tisk_cislo_6a(lo);
  } else {
    tisk_cislo_6(lo);
  }
  printf("  ");
#endif
}

void tisk_read() {
#ifdef DETAIL_PRINT
  printf("Read op:   ");
#endif
  tisk_counter(read_ops);
}

void tisk_cache1() {
#ifdef DETAIL_PRINT
  printf("Miss L1:   ");
#endif
  tisk_counter(cache1_miss);
}

void tisk_cache2() {
#ifdef DETAIL_PRINT
  printf("Miss L2:   ");
#endif
  tisk_counter(cache2_miss);
}

void tisk_cache3() {
#ifdef DETAIL_PRINT
  printf("Miss L3:   ");
#endif
  tisk_counter(cache3_miss);
}

void cache_print() {
  tisk_read();
  if (cache_l3 != NULL)
    tisk_cache3();
  if (cache_l2 != NULL)
    tisk_cache2();
  if (cache_l1 != NULL)
    tisk_cache1();
}

void cache_clear() {
  nuluj_counter(read_ops);
  nuluj_counter(nulovy);
  nuluj_counter(cache1_miss);
  nuluj_counter(cache2_miss);
  nuluj_counter(cache3_miss);
}

void cache_reset() {
  ulong i, j;

  i = CACHE_H1 * CACHE_S1;
  if (i != 0) {
    for (j = 0; j < i; j++)
      cache_l1[j].addr = j + CACHE_S1;
    for (j = 0; j < i; j++)
      cache_l1[j].rank = 1 + j;
  }
  i = CACHE_H2 * CACHE_S2;
  if (i != 0) {
    for (j = 0; j < i; j++)
      cache_l2[j].addr = j + CACHE_S2;
    for (j = 0; j < i; j++)
      cache_l2[j].rank = 1 + j;
  }
  i = CACHE_H3 * CACHE_S3;
  if (i != 0) {
    for (j = 0; j < i; j++)
      cache_l3[j].addr = j + CACHE_S3;
    for (j = 0; j < i; j++)
      cache_l3[j].rank = 1 + j;
  }
  cache_clear();
}

void cache_init(ulong z1, ulong z2, ulong z3, ulong z4, ulong z5, ulong z6, ulong z7,
                ulong z8, ulong z9) {
  ulong i, j;

  cache_l1 = NULL;
  cache_l2 = NULL;
  cache_l3 = NULL;

  CACHE_B1 = z1;
  CACHE_H1 = z2;
  CACHE_S1 = z3;
  CACHE_B2 = z4;
  CACHE_H2 = z5;
  CACHE_S2 = z6;
  CACHE_B3 = z7;
  CACHE_H3 = z8;
  CACHE_S3 = z9;

  read_ops = alok_counter();
  cache1_miss = alok_counter();
  cache2_miss = alok_counter();
  cache3_miss = alok_counter();
  nulovy = alok_counter();

  i = CACHE_B1 * CACHE_H1 * CACHE_S1;
  if (i != 0) {
#ifdef DETAIL_PRINT
    printf("Cache L1 initialized. \n");
    printf("Size: %lu K    h: %lu    block size: %lu    s: %lu\n", i / 1024l,
           CACHE_H1, CACHE_B1, CACHE_S1);
#endif
    i = CACHE_H1 * CACHE_S1;
    cache_l1 = (cache_line *)alokace(i, sizeof(struct cache_line));
  }
  i = CACHE_B2 * CACHE_H2 * CACHE_S2;
  if (i != 0) {
#ifdef DETAIL_PRINT
    printf("Cache L2 initialized. \n");
    printf("Size: %lu K    h: %lu    block size: %lu    s: %lu\n", i / 1024l,
           CACHE_H2, CACHE_B2, CACHE_S2);
#endif
    i = CACHE_H2 * CACHE_S2;
    cache_l2 = (cache_line *)alokace(i, sizeof(struct cache_line));
  }
  i = CACHE_B3 * CACHE_H3 * CACHE_S3;
  if (i != 0) {
#ifdef DETAIL_PRINT
    printf("Cache L3 initialized. \n");
    printf("Size: %lu K    h: %lu    block size: %lu    s: %lu\n", i / 1024l,
           CACHE_H3, CACHE_B3, CACHE_S3);
#endif
    i = CACHE_H3 * CACHE_S3;
    cache_l3 = (cache_line *)alokace(i, sizeof(struct cache_line));
  }
  cache_reset();
}

void cache_delete() {
  free(cache_l3);
  free(cache_l2);
  free(cache_l1);

  free(read_ops);
  free(cache1_miss);
  free(cache2_miss);
  free(cache3_miss);
  free(nulovy);
}

void cache_reads(ulong x, ulong b, ulong h, ulong s, struct cache_line *cache,
                 t_counter c) {
  /*
    read inside one level of cache
  */
  ulong ktery, maxim, maxim_kde;
  ulong a, a2;
  ulong i;
  a = x / b;
  a2 = a % h;

  maxim_kde = 0l;
  ktery = s;
  maxim = cache[a2 * s].rank;
  for (i = 0; i < s; i++) {
    if (cache[a2 * s + i].addr == a)
      ktery = i;
    if (maxim < cache[a2 * s + i].rank) {
      maxim = cache[a2 * s + i].rank;
      maxim_kde = i;
    }
  }
  for (i = 0; i < s; i++) {
    cache[a2 * s + i].rank++;
  }

  if (ktery == s) {
    // cache miss
    cache[a2 * s + maxim_kde].rank = 0l;
    cache[a2 * s + maxim_kde].addr = a;
    zvys_counter(c, 1l);
  } else {
    // cache hit
    cache[a2 * s + ktery].rank = 0l;
  }
}

void cache_read(void *y) {
  /*
    read for cache hierarchy
  */
  ulong x;

  x = (ulong)y;
  zvys_counter(read_ops, 1l);
  if (cache_l1 != NULL)
    cache_reads(x, CACHE_B1, CACHE_H1, CACHE_S1, cache_l1, cache1_miss);
  if (cache_l2 != NULL)
    cache_reads(x, CACHE_B2, CACHE_H2, CACHE_S2, cache_l2, cache2_miss);
  if (cache_l3 != NULL)
    cache_reads(x, CACHE_B3, CACHE_H3, CACHE_S3, cache_l3, cache3_miss);
}

void cache_read_d(void *y) {
  /*
    read operand, but it doesn't count into statistics
  */
  ulong x;

  x = (ulong)y;

  if (cache_l1 != NULL)
    cache_reads(x, CACHE_B1, CACHE_H1, CACHE_S1, cache_l1, nulovy);
  if (cache_l2 != NULL)
    cache_reads(x, CACHE_B2, CACHE_H2, CACHE_S2, cache_l2, nulovy);
  if (cache_l3 != NULL)
    cache_reads(x, CACHE_B3, CACHE_H3, CACHE_S3, cache_l3, nulovy);
}

void cache_read_r(void *y) {
  /*
    read once (doesnt't get operand into the CA
  */
  zvys_counter(read_ops, 1l);
}
