void mv_coo_f(float *a, float *x, float *y, uint *x_pos, uint *y_pos, ullong n, ullong nnz) {
  // for float  Ax->y (32bit coordinates)
  ullong i, j, ii, lj, uj;
  uint xp, yp;

  for (i = 0; i < n; i++) {
    y[i] = 0.0;
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    y[yp] += a[i] * x[xp];
  }
}

void fused_coo_f(float *a, float *x, float *y, float *y2, uint *x_pos, uint *y_pos, ullong n, ullong nnz) {
  // for float  Ax->y Ax2->y2 (32bit coordinates)
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    y[i] = y2[i] = 0.0;
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    ai = a[i];
    y[yp] += ai * x[xp];
    y2[xp] += ai * x[yp];
  }
}


void mv_coo_f16S(float *a, float *x, float *  __restrict__ y, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y (16bit coordinates) with setting
  uint i, j, ii, lj, uj;
  usint xp, yp;

  for (i = 0; i < n; i++) {
    y[i] = 0.0;
    //cache_read(y + i);
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    //cache_read(x_pos + i);
    //cache_read(y_pos + i);
    y[yp] += a[i] * x[xp];
    //cache_read(a + i);
    //cache_read(x + xp);
    //cache_read(y + yp);
  }
}


void mv_coo_f16A(float *a, float *x, float *  __restrict__ y, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // pro float  Ax->y  (16bit coordinates) with addition
  uint i, j, ii, lj, uj;
  usint xp, yp;

  /*for (i = 0; i < n; i++) {
    y[i] = 0.0;
    //cache_read(y + i);
  } */
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    //cache_read(x_pos + i);
    //cache_read(y_pos + i);
    y[yp] += a[i] * x[xp];
    //cache_read(a + i);
    //cache_read(x + xp);
    //cache_read(y + yp);
  }
}

void fused_coo_f16SS(float *a, float *x, float *  __restrict__ y, float *  __restrict__ y2, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y Ax2->y2 (16bit coordinates) with setting  y,y2
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    y[i] = y2[i] = 0.0;
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    ai = a[i];
    y[yp] += ai * x[xp];
    y2[xp] += ai * x[yp];
  }
}


void fused_coo_f16AS(float *a, float *x, float *  __restrict__ y, float *  __restrict__ y2, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // pro float  Ax->y Ax2->y2  (16bit coordinates); y2 settintg,y addition
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    //y[i] = 
    y2[i] = 0.0;
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    ai = a[i];
    y[yp] += ai * x[xp];
    y2[xp] += ai * x[yp];
  }
}


void fused_coo_f16SA(float *a, float *x, float *  __restrict__ y, float *  __restrict__ y2, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y Ax2->y2 (16bit coordinates); y setting,y2 addition
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    y[i] =  0.0;  //y2[i] =
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    ai = a[i];
    y[yp] += ai * x[xp];
    y2[xp] += ai * x[yp];
  }
}

void fused_coo_f16AA(float *a, float *x1, float *  __restrict__ y1, float *  __restrict__ y2, usint *x_pos, usint *y_pos, uint n, uint nnz,float *x2) {
  // for float  Ax->y Ax2->y2  (16bit coordinates); y,y2 addition
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  /*for (i = 0; i < n; i++) {
    //y[i] = 
    y2[i] = 0.0;
  } */
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    ai = a[i];
    y1[yp] += ai * x1[xp];
    y2[xp] += ai * x2[yp];
  }
}

void mv_coo_f_cache(float *a, float *x, float *  __restrict__ y, uint *x_pos, uint *y_pos, ullong n, ullong nnz) {
  // for float  Ax->y  (32bit coordinates), cache
  ullong i, j, ii, lj, uj;
  uint xp, yp;

  for (i = 0; i < n; i++) {
    y[i] = 0.0;
    cache_read(y + i);
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    y[yp] += a[i] * x[xp];
    cache_read(a + i);
    cache_read(x + xp);
    cache_read(y + yp);
  }
}

void fused_coo_f_cache(float *a, float *x, float *y, float *y2, uint *x_pos,uint *y_pos, ullong n, ullong nnz) {
  // pro float  Ax->y Ax2->y2  (32bit coordinates), cache
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    y[i] = y2[i] = 0.0;
    cache_read(y + i);
    cache_read(y2 + i);
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    ai = a[i];
    cache_read(a + i);
    y[yp] += ai * x[xp];
    cache_read(x + xp);
    cache_read(y + yp);
    y2[xp] += ai * x[yp];
    cache_read(x + yp);
    cache_read(y2 + xp);
  }
}



void mv_coo_f_cache16S(float *a, float *x, float *y, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y (16bit coordinates), cache
  uint i, j, ii, lj, uj;
  usint xp, yp;

  for (i = 0; i < n; i++) {
    y[i] = 0.0;
    cache_read(y + i);
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    y[yp] += a[i] * x[xp];
    cache_read(a + i);
    cache_read(x + xp);
    cache_read(y + yp);
  }
}


void mv_coo_f_cache16A(float *a, float *x, float *y, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y (16bit coordinates),cache
  uint i, j, ii, lj, uj;
  usint xp, yp;

  /*for (i = 0; i < n; i++) {
    y[i] = 0.0;
    cache_read(y + i);
  } */
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    y[yp] += a[i] * x[xp];
    cache_read(a + i);
    cache_read(x + xp);
    cache_read(y + yp);
  }
}


void fused_coo_f_cache16SS(float *a, float *x, float *y, float *y2, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y Ax2->y2 (16bit coordinates),cache
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    y[i] = y2[i] = 0.0;
    cache_read(y + i);
    cache_read(y2 + i);
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    ai = a[i];
    cache_read(a + i);
    y[yp] += ai * x[xp];
    cache_read(x + xp);
    cache_read(y + yp);
    y2[xp] += ai * x[yp];
    cache_read(x + yp);
    cache_read(y2 + xp);
  }
}


void fused_coo_f_cache16AS(float *a, float *x, float *y, float *y2, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y Ax2->y2 (16bit coordinates),cache
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    //y[i] = 
    y2[i] = 0.0;
    //cache_read(y + i);
    cache_read(y2 + i);
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    ai = a[i];
    cache_read(a + i);
    y[yp] += ai * x[xp];
    cache_read(x + xp);
    cache_read(y + yp);
    y2[xp] += ai * x[yp];
    cache_read(x + yp);
    cache_read(y2 + xp);
  }
}


void fused_coo_f_cache16SA(float *a, float *x, float *y, float *y2, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y Ax2->y2 (16bit coordinates),cache
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  for (i = 0; i < n; i++) {
    //y2[i] =
    y[i] =  0.0;
    cache_read(y + i);
    //cache_read(y2 + i);
  }
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    ai = a[i];
    cache_read(a + i);
    y[yp] += ai * x[xp];
    cache_read(x + xp);
    cache_read(y + yp);
    y2[xp] += ai * x[yp];
    cache_read(x + yp);
    cache_read(y2 + xp);
  }
}


void fused_coo_f_cache16AA(float *a, float *x, float *y, float *y2, usint *x_pos, usint *y_pos, uint n, uint nnz) {
  // for float  Ax->y Ax2->y2 (16bit coordinates),cache
  ullong i, j, ii, lj, uj;
  uint xp, yp;
  float ai;

  /*for (i = 0; i < n; i++) {
    //y[i] = 
    y2[i] = 0.0;
    //cache_read(y + i);
    cache_read(y2 + i);
  } */
  for (i = 0; i < nnz; i++) {
    xp = x_pos[i];
    yp = y_pos[i];
    cache_read(x_pos + i);
    cache_read(y_pos + i);
    ai = a[i];
    cache_read(a + i);
    y[yp] += ai * x[xp];
    cache_read(x + xp);
    cache_read(y + yp);
    y2[xp] += ai * x[yp];
    cache_read(x + yp);
    cache_read(y2 + xp);
  }
}