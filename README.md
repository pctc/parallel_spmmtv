# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This archive contains source codes for the article:

Multilayer Approach for Joint Direct and Transposed Sparse Matrix Vector Multiplication for Multithreaded CPUs

AUTHORS: Ivan Simecek, Ivan Kotenkov and Daniel Langr

* Version 0.1

### How do I get set up? ###

* Summary of set up
compilation (for AVX instruction set): g++ -Ofast -ffast-math -mavx -fopenmp cache_anal_block.cpp

* Configuration (command line parameters):
<.exe> <filename = sparse matrix in MTX format> <nop> <no. of threads> <block thr.>

<nop> is =2 (for just a structure of sparse matrix), =3 (for real sparse matrix), =4 (for complex sparse matrix),

<block thr.> is real number from <0,1], parameter alpha

* Dependencies:
OpenMP, Boost

* Database configuration
* How to run tests

example: ./a.out ../amazon0312_2.mtx 2 12 0.01 >v_th12.txt

* Deployment instructions

### Contribution guidelines ###

* TODO:
revision of the code: make code more clear

support of double using templates

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Ivan Simecek, xsimecek@fit.cvut.cz

* Other community or team contact