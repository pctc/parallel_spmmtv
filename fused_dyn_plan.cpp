/*

for execution by plan (with mutexes)

BACKWARDS = the main loop in opposite direction
typ&3: 0 =all rows
       1 = skip head and tail
       2 = skip all empty  
*/

#ifdef BACKWARDS
void fused_dyn_planb(float *x, float *rest y, float *rest y2, uint n,int typ)
#else
void fused_dyn_plan(float *x, float *rest y, float *rest y2, uint n,int typ)
#endif
{
    int i;
  uint l,executed=0;
  double start,start2,end,end2;
  uint podil=(n)/threads;//(n+threads-1)/threads;
  omp_lock_t mutexx[MAX_BLO];
  omp_lock_t mutexy[MAX_BLO];

          start=omp_get_wtime();

          //std::cout << " CSR " << end-start << std::endl;

  
  for(l=0;l<noboundsx;l++) omp_init_lock(mutexx+l);
  for(l=0;l<noboundsy;l++) omp_init_lock(mutexy+l);
  int podil2=no_blocks/threads; 
  #pragma omp parallel num_threads(threads) private(end2,i) shared(start,start2,executed,pole_exec)         
  {

    uint tid,j,kx,ky;

    tid=omp_get_thread_num();
            //std::cout << " t0=" << tid << std::endl;
    memset(y+tid*podil,0,podil*4);
    memset(y2+tid*podil,0,podil*4);
     //for(uint i=0;i<n;i++) y[i]=0.0;
    //memset(y,0,n*4);
    #pragma omp barrier
            //std::cout << " t0a=" << tid << std::endl;
    #pragma omp master
       start2=omp_get_wtime();
    //
     
    /*std::stringstream msg1;
    msg1 << " t1=" << tid << " i2=" << plan_thr_num[tid] << "..." << plan_thr_num[tid+1]<< std::endl; 
    std::cout << msg1.str();
    fflush(stdout);  */
    #ifdef BACKWARDS
    for(int i2=plan_thr_num[tid+1]-1;(i2>=plan_thr_num[tid])&&(i2>=0);i2--)  
    #else
    for(int i2=plan_thr_num[tid];i2<plan_thr_num[tid+1];i2++)   
    #endif
    {
        i=plan_thr_reg[i2];
        //#pragma omp single

        /*std::stringstream msg2;
        msg2 << " tid=" << tid << " i2=" << i2 << " i=" << i<< std::endl; 
        std::cout << msg2.str();
        fflush(stdout);     */
        ky=0;
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++) omp_set_lock(mutexy+j);
        kx=0;
        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++) omp_set_lock(mutexx+j);
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif
          
          switch(typ&3){
          case 0: fused_csr_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, x+pole_pol[i].starty);
          break;
          case 1: fused_csr_f16AA2(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, pole_pol[i].lower, pole_pol[i].upper, x+pole_pol[i].starty);
          break;
          case 2: fused_csr_f16AA3(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, pole_pol[i].lower, pole_pol[i].upper, x+pole_pol[i].starty);
          break;
          }  
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif

          fused_coo_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty, x+pole_pol[i].starty); 
          //fused_coo_f16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }

        /* std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */
        //std::cout << " t3=" << tid << " i2=" << i2 << " i=" << i<< std::endl;        
        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++)
          omp_unset_lock(mutexx+j);
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          omp_unset_lock(mutexy+j);
        #pragma omp atomic
          executed++;
        //std::cout << " t4=" << tid << " i2=" << i2 << " i=" << i<< std::endl;          
        
   }//end of for     
          #ifdef TIMER3
          end2=omp_get_wtime();
          std::cout << " FUSSED dyn " << tid << " = "<< end2-start2 << std::endl;
          //std::cout  << tid << std::endl;        
          #endif         
      
      }  // end of par.
      
 #ifdef TIMER2
 end=omp_get_wtime();
 std::cout << " FUSSED " << end-start << std::endl;        
 #endif   
 #ifdef BACKWARDS     
 std::cout << " FUSSED plan back" << executed << std::endl; 
 #else
  std::cout << " FUSSED plan" << executed << std::endl; 
 #endif
 return;
}