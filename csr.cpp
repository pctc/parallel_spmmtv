void mv_csr_f(float *a, float *x, float *y, ullong *adr, uint *ci, ullong n) {
  // for float   Ax->y   (32 bit)
  ullong i, j, ii, lj, uj;
  double start,end;
  float s;
          #ifdef TIMER2
          start=omp_get_wtime();
          //std::cout << " CSR " << end-start << std::endl;
          #endif
  lj = adr[0];
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      s += a[j] * x[ii];
    }
    y[i] = s;
    lj = uj;
  }
      
          #ifdef TIMER2
          end=omp_get_wtime();
          std::cout << " SPMV csr " << end-start << std::endl;        
          #endif   
}


void fused_csr_f(float *a, float *x, float *y, float *y2, ullong *adr, uint *ci, ullong n) {
  // for float  Ax->y Ax2->y2 (32 bit)
  ullong i, j, ii, lj, uj;
  double start,end;  
  float s, xi, aj;
          #ifdef TIMER2
          start=omp_get_wtime();
          //std::cout << " CSR " << end-start << std::endl;
          #endif
  for (i = 0; i < n; i++) {
    y2[i] = 0.0;
  }
  lj = adr[0];
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    xi = x[i];
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      aj = a[j];
      s += aj * x[ii];
      // ii, i
      y2[ii] += xi * aj;
    }
    y[i] = s;
    lj = uj;
  }
          #ifdef TIMER2
          end=omp_get_wtime();
          std::cout << " FUSED csr " << end-start << std::endl;        
          #endif     
}


void mv_csr_f16S(float *a, float *x, float *  __restrict__ y, uint *adr, usint *ci, uint n) {
  // for float   Ax->y (16 bit), setting y
  uint i, j, ii, lj, uj;
  float s;

  lj = adr[0];
  //cache_read(adr);
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    //cache_read(adr + i + 1);
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      //cache_read(ci + j);
      s += a[j] * x[ii];
      //cache_read(a + j);
      //cache_read(x + ii);
    }
    y[i] = s;
    lj = uj;
    //cache_read(y + i);
  }
}


void mv_csr_f16A(float *a, float *x, float *  __restrict__ y, uint *adr, usint *ci, uint n) {
  // for float   Ax->y (16 bit), addition y
  uint i, j, ii, lj, uj;
  float s;

  lj = adr[0];
  //cache_read(adr);
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    //cache_read(adr + i + 1);
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      //cache_read(ci + j);
      s += a[j] * x[ii];
      //cache_read(a + j);
      //cache_read(x + ii);
    }
    y[i] += s;
    lj = uj;
    //cache_read(y + i);
  }
}


void fused_csr_f16SS(float *a, float *x, float *  __restrict__ y, float *  __restrict__ y2, uint *adr, usint *ci, uint n) {
  // for float  Ax->y Ax2->y2  (16 bit), setting y,y2
  uint i, j, ii, lj, uj;
  float s, xi, aj;

  for (i = 0; i < n; i++) {
    y2[i] = 0.0;
  }
  lj = adr[0];
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    xi = x[i];
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      aj = a[j];
      s += aj * x[ii];
      // ii, i
      y2[ii] += xi * aj;
    }
    y[i] = s;
    lj = uj;
  }
}

void fused_csr_f16AS(float *a, float *x, float *  __restrict__ y, float *  __restrict__ y2, uint *adr, usint *ci, uint n) {
  // for float  Ax->y Ax2->y2 (16 bit), setting y2, addition y
  uint i, j, ii, lj, uj;
  float s, xi, aj;

  for (i = 0; i < n; i++) {
    y2[i] = 0.0;
  }
  lj = adr[0];
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    xi = x[i];
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      aj = a[j];
      s += aj * x[ii];
      // ii, i
      y2[ii] += xi * aj;
    }
    y[i] += s;
    lj = uj;
  }
}


void fused_csr_f16SA(float *a, float *x, float *  __restrict__ y, float *  __restrict__ y2, uint *adr, usint *ci, uint n) {
  // for float  Ax->y Ax2->y2  (16 bit), setting y, addition y2
  uint i, j, ii, lj, uj;
  float s, xi, aj;

  /*for (i = 0; i < n; i++) {
    y2[i] = 0.0;
  } */
  lj = adr[0];
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    xi = x[i];
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      aj = a[j];
      s += aj * x[ii];
      // ii, i
      y2[ii] += xi * aj;
    }
    y[i] = s;
    lj = uj;
  }
}

void fused_csr_f16AA(float *a, float *x1, float *  __restrict__ y1, float *  __restrict__ y2, uint *adr, usint *ci, uint n, float *x2) {
  // for float  Ax->y Ax2->y2  (16 bit), addition y,y2
  uint i, j, ii, lj, uj;
  float s, xi, aj;

  /*for (i = 0; i < n; i++) {
    y2[i] = 0.0;
  } */
  lj = adr[0];
  for (i = 0; i < n; i++) {
    //std::cout << "i= "<< i<< std::endl;
    uj = adr[i + 1];
    //std::cout << " j= "<< lj << "/"<< uj <<std::endl;
    xi = x2[i];
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      //std::cout << " ii= "<< ii <<std::endl;
      aj = a[j];
      //std::cout << " aj= "<< aj <<std::endl;

      s += aj * x1[ii];
      // ii, i
      y2[ii] += xi * aj;
    }
    y1[i] += s;
    lj = uj;
  }
}

void fused_csr_f16AA2(float *a, float *x1, float *  __restrict__ y1, float *  __restrict__ y2, uint *adr, usint *ci, uint n,uint lower,uint upper, float *x2) {
  // for float  Ax->y Ax2->y2  (16 bit), addition y,y2 remove heading and tailing zeroes
  uint i, j, ii, lj, uj;
  float s, xi, aj;

  /*for (i = 0; i < n; i++) {
    y2[i] = 0.0;
  } */
  lj = adr[lower];
  for (i = lower; i < upper; i++) {
    uj = adr[i + 1];
    xi = x2[i];
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      aj = a[j];
      s += aj * x1[ii];
      // ii, i
      y2[ii] += xi * aj;
    }
    y1[i] += s;
    lj = uj;
  }
}

void fused_csr_f16AA3(float *a, float *x1, float *  __restrict__ y1, float *  __restrict__ y2, uint *adr, usint *ci, uint n,uint lower,uint upper, float *x2) {
  // for float  Ax->y Ax2->y2  (16 bit), addition y,y2 remove heading and tailing and imm. zeroes
  uint i, j, ii, lj, uj;
  float s, xi, aj;

  /*for (i = 0; i < n; i++) {
    y2[i] = 0.0;
  } */
  //std::cout << "zde" << std::endl;
  lj = adr[lower];
  for (i = lower; i < upper; i++) {
    uj = adr[i + 1];
    if (lj==uj) continue;
    xi = x2[i];
      ii = ci[lj];
      aj = a[lj];
      s = aj * x1[ii];
      // ii, i
      y2[ii] += xi * aj;

    for (j = lj+1; j < uj; j++) {
      ii = ci[j];
      aj = a[j];
      s += aj * x1[ii];
      // ii, i
      y2[ii] += xi * aj;
    }
    y1[i] += s;
    lj = uj;
  }
  //std::cout << "end " << std::endl;  
}


void mv_csr_f_cache(float *a, float *x, float *y, ullong *adr, uint *ci, ullong n) {
  // for float   Ax->y,cache
  ullong i, j, ii, lj, uj;
  float s;

  lj = adr[0];
  cache_read(adr);
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    cache_read(adr + i + 1);
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      cache_read(ci + j);
      s += a[j] * x[ii];
      cache_read(a + j);
      cache_read(x + ii);
    }
    y[i] = s;
    lj = uj;
    cache_read(y + i);
  }
}


void fused_csr_f_cache(float *a, float *x, float *y, float *y2, ullong *adr, uint *ci, ullong n) {
  // for float  Ax->y Ax2->y2 ,cache
  ullong i, j, ii, lj, uj;
  float s, xi, aj;

  for (i = 0; i < n; i++) {
    y2[i] = 0.0;
    cache_read(y2 + i);
  }
  lj = adr[0];
  cache_read(adr);
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    cache_read(adr + i + 1);
    xi = x[i];
    cache_read(x + i);
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      cache_read(ci + j);
      aj = a[j];
      s += aj * x[ii];
      cache_read(a + j);
      cache_read(x + ii);
      // ii, i
      y2[ii] += xi * aj;
      cache_read(y2 + ii);
    }
    y[i] = s;
    lj = uj;
    cache_read(y + i);
  }
}


void mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) {
  // for float   Ax->y ,cache
  uint i, j, ii, lj, uj;
  float s;

  lj = adr[0];
  cache_read(adr);
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    cache_read(adr + i + 1);
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      cache_read(ci + j);
      s += a[j] * x[ii];
      cache_read(a + j);
      cache_read(x + ii);
    }
    y[i] = s;
    lj = uj;
    cache_read(y + i);
  }
}


void fused_csr_f_cache16A(float *a, float * x, float * y, float *y2, uint *adr,usint *ci, uint n) {
  // for float  Ax->y Ax2->y2 ,cache
  uint i, j, ii, lj, uj;
  float s, xi, aj;

/*  for (i = 0; i < n; i++) {
    y2[i] = 0.0;
    cache_read(y2 + i);
  } */
  lj = adr[0];
  cache_read(adr);
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    cache_read(adr + i + 1);
    xi = x[i];
    cache_read(x + i);
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      cache_read(ci + j);
      aj = a[j];
      s += aj * x[ii];
      cache_read(a + j);
      cache_read(x + ii);
      // ii, i
      y2[ii] += xi * aj;
      cache_read(y2 + ii);
    }
    y[i] = s;
    lj = uj;
    cache_read(y + i);
  }
}


void fused_csr_f_cache16S(float *a, float *x, float *y, float *y2, uint *adr,usint *ci, uint n) {
  // for float  Ax->y Ax2->y2,cache
  uint i, j, ii, lj, uj;
  float s, xi, aj;

  for (i = 0; i < n; i++) {
    y2[i] = 0.0;
    cache_read(y2 + i);
  }
  lj = adr[0];
  cache_read(adr);
  for (i = 0; i < n; i++) {
    uj = adr[i + 1];
    cache_read(adr + i + 1);
    xi = x[i];
    cache_read(x + i);
    s = 0.0;
    for (j = lj; j < uj; j++) {
      ii = ci[j];
      cache_read(ci + j);
      aj = a[j];
      s += aj * x[ii];
      cache_read(a + j);
      cache_read(x + ii);
      // ii, i
      y2[ii] += xi * aj;
      cache_read(y2 + ii);
    }
    y[i] = s;
    lj = uj;
    cache_read(y + i);
  }
}