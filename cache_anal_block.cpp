/*
 * usage: ./block <filename> <n> <p>
 * <filename> - matrix file in Matrix Market format
 * if <p> is zero, block size = n
 * else            block size = 2^n
 *
 * Do not check matrix symmetry!
 */
#include <stdio.h>
#include <stdlib.h>
  #include <math.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <string.h>
#include <stdint.h>
#include <omp.h>
//#include <boost/dynamic_bitset.hpp>
//#include <boost/lexical_cast.hpp>
#define usint uint16_t
#define uint uint32_t
#define ullong uint64_t
#define MAX_BLO 25000 // max. # of regions
#define MAX_THR 20    // max. # of threads
//#define TIMER2
//#define TIMER3
//#define SKIP1
//#define CHECK1
#define rest __restrict__

uint *pole_x;
uint *pole_y;
ullong *pole_csr;

ullong mm; // number of rows
ullong nn; // number of columns
ullong zz; // number of nonzeros
uint b_no; // number of  blocks
uint bs; // max. block size
uint no_blocks; // no. of blocks
uint threads; // no of threads
uint noboundsx,noboundsy; // no. of boundaries
float block_thr; // block threshold
// 

struct polozka {
  uint startx, starty;
  uint sizex,sizey;
  uint lower,upper;
  float *a; // value of a
  usint *x; // value of x
  void *temp; // either CSR (addr) or COO(Y)
  uint mx1,mx2;
  uint my1,my2;
  ullong pocty,kod;
};

struct polozka *pole_pol;

struct exec {
  char done;
//  ullong time;
};

volatile struct exec *pole_exec;

uint plan_thr_num[MAX_THR];
uint plan_thr_reg[MAX_BLO];

int *flush_cache;

//#include "cache_8K_128K_8MB.def"
#include "cache_sim2.inc"

static const uint64_t MortonTable256[256] = {
    0x0000, 0x0001, 0x0004, 0x0005, 0x0010, 0x0011, 0x0014, 0x0015, 0x0040,
    0x0041, 0x0044, 0x0045, 0x0050, 0x0051, 0x0054, 0x0055, 0x0100, 0x0101,
    0x0104, 0x0105, 0x0110, 0x0111, 0x0114, 0x0115, 0x0140, 0x0141, 0x0144,
    0x0145, 0x0150, 0x0151, 0x0154, 0x0155, 0x0400, 0x0401, 0x0404, 0x0405,
    0x0410, 0x0411, 0x0414, 0x0415, 0x0440, 0x0441, 0x0444, 0x0445, 0x0450,
    0x0451, 0x0454, 0x0455, 0x0500, 0x0501, 0x0504, 0x0505, 0x0510, 0x0511,
    0x0514, 0x0515, 0x0540, 0x0541, 0x0544, 0x0545, 0x0550, 0x0551, 0x0554,
    0x0555, 0x1000, 0x1001, 0x1004, 0x1005, 0x1010, 0x1011, 0x1014, 0x1015,
    0x1040, 0x1041, 0x1044, 0x1045, 0x1050, 0x1051, 0x1054, 0x1055, 0x1100,
    0x1101, 0x1104, 0x1105, 0x1110, 0x1111, 0x1114, 0x1115, 0x1140, 0x1141,
    0x1144, 0x1145, 0x1150, 0x1151, 0x1154, 0x1155, 0x1400, 0x1401, 0x1404,
    0x1405, 0x1410, 0x1411, 0x1414, 0x1415, 0x1440, 0x1441, 0x1444, 0x1445,
    0x1450, 0x1451, 0x1454, 0x1455, 0x1500, 0x1501, 0x1504, 0x1505, 0x1510,
    0x1511, 0x1514, 0x1515, 0x1540, 0x1541, 0x1544, 0x1545, 0x1550, 0x1551,
    0x1554, 0x1555, 0x4000, 0x4001, 0x4004, 0x4005, 0x4010, 0x4011, 0x4014,
    0x4015, 0x4040, 0x4041, 0x4044, 0x4045, 0x4050, 0x4051, 0x4054, 0x4055,
    0x4100, 0x4101, 0x4104, 0x4105, 0x4110, 0x4111, 0x4114, 0x4115, 0x4140,
    0x4141, 0x4144, 0x4145, 0x4150, 0x4151, 0x4154, 0x4155, 0x4400, 0x4401,
    0x4404, 0x4405, 0x4410, 0x4411, 0x4414, 0x4415, 0x4440, 0x4441, 0x4444,
    0x4445, 0x4450, 0x4451, 0x4454, 0x4455, 0x4500, 0x4501, 0x4504, 0x4505,
    0x4510, 0x4511, 0x4514, 0x4515, 0x4540, 0x4541, 0x4544, 0x4545, 0x4550,
    0x4551, 0x4554, 0x4555, 0x5000, 0x5001, 0x5004, 0x5005, 0x5010, 0x5011,
    0x5014, 0x5015, 0x5040, 0x5041, 0x5044, 0x5045, 0x5050, 0x5051, 0x5054,
    0x5055, 0x5100, 0x5101, 0x5104, 0x5105, 0x5110, 0x5111, 0x5114, 0x5115,
    0x5140, 0x5141, 0x5144, 0x5145, 0x5150, 0x5151, 0x5154, 0x5155, 0x5400,
    0x5401, 0x5404, 0x5405, 0x5410, 0x5411, 0x5414, 0x5415, 0x5440, 0x5441,
    0x5444, 0x5445, 0x5450, 0x5451, 0x5454, 0x5455, 0x5500, 0x5501, 0x5504,
    0x5505, 0x5510, 0x5511, 0x5514, 0x5515, 0x5540, 0x5541, 0x5544, 0x5545,
    0x5550, 0x5551, 0x5554, 0x5555};

uint64_t get_morton(unsigned int x, unsigned int y) {
  uint64_t z;
  z = MortonTable256[(y >> 24)] << 49 | MortonTable256[(x >> 24)] << 48;
  z |= MortonTable256[(y >> 16) & 0xFF] << 33 |
       MortonTable256[(x >> 16) & 0xFF] << 32;
  z |= MortonTable256[(y >> 8) & 0xFF] << 17 |
       MortonTable256[(x >> 8) & 0xFF] << 16;
  z |= MortonTable256[y & 0xFF] << 1 | MortonTable256[x & 0xFF];
  return z;
}


// compare functions for reordering of regions
int compare_pol_mor(const void *a, const void *b) {
  // return ( ((struct list *)a)->kod - ((struct list *)b)->kod );
  struct polozka xa, xb;
  xa = *(struct polozka *)a;
  xb = *(struct polozka *)b;
  if (xa.kod > xb.kod)
    return 1;
  return -1;
}


int compare_uint(const void *a, const void *b) {
  // return ( ((struct list *)a)->kod - ((struct list *)b)->kod );
  uint xa, xb;
  xa = *(uint *)a;
  xb = *(uint *)b;
  if (xa > xb)
    return 1;
  return -1;
}


int compare_pol_lex(const void *a, const void *b) {
  // return ( ((struct list *)a)->kod - ((struct list *)b)->kod );
  struct polozka xa, xb;
  xa = *(struct polozka *)a;
  xb = *(struct polozka *)b;
  if (xa.starty > xb.starty)
    return 1;
  if (xa.starty < xb.starty)    
    return -1;
  if (xa.startx > xb.startx)
    return 1;
  return -1;    
}


void swap_uint(uint *a, uint *b) {
  uint c;
  c = *a;
  *a = *b;
  *b = c;
}

float* alokacef (long a)
/*
  allocate a elements of type double
*/
{
  float *b;
  b= (float*) calloc (a,sizeof (float));
  if (b==NULL){
    printf ("\n Insufficient memory.\n");
    abort ();
  }
  return b;
}

void* alokace (long a)
/*
  allocate a elements of type byte
*/
{
  void *b;
  b=malloc (a);
  if (b==NULL){
    printf ("\n Insufficient memory.\n");
    abort ();
  }
  return b;
}


int vyprazdni(int *temp,int k)
{
  // flush cache
  int i,s=0;
  for(i=0;i<k;i++) s+=temp[i];
  return s;
} 


void do_csc( uint *ci, uint n,ullong nnz, ullong *pole_csc) {

   // for CSR to CSC
  ullong sum,temp,i;
  for (i = 0; i < nnz; i++) {
    pole_csc[ci[i]]++;  
  }
  sum=0;
  for(i=0;i<=n;i++)
  {
    temp=pole_csc[i];
    pole_csc[i]=sum;
    sum+=temp;
  }
}


void do_csc16( usint *ci, uint n,ullong nnz, uint *pole_csc) {

  // for CSR16 to CSC16
  ullong sum,temp,i;  
  for (i = 0; i <= n; i++)
    pole_csc[i] = 0;  
  for (i = 0; i < nnz; i++) {
    pole_csc[ci[i]]++;  
  }
  sum=0;
  for(i=0;i<=n;i++)
  {
    temp=pole_csc[i];
    pole_csc[i]=sum;
    sum+=temp;
  }
}


int najdi_hranice(uint kolik,ullong N,uint n,ullong *addr,uint *kam) 
{
  // boundaries for large regions
  unsigned int i,lo,hi,kde_min,pocet;
  ullong number,kolik_min,index;
  
  kam[0]=0;
  i=1;
  while(kde_min<n)
  {
  number=i*N/kolik;
  if (i==0)
  {  lo=0; hi=bs; if (hi>n) hi=n;}
  else
  {  lo=kam[i-1]+1; hi=kam[i-1]+bs; if (hi>n) hi=n;}
  while ((hi-lo)>5)
  {
    index=(hi+lo)/2;
    if (addr[index]>number) hi=index;
    else lo=index;
  }
  
  kolik_min=abs(addr[lo]-number);
  kde_min=lo;
  lo++;
  for(;lo<=hi;lo++)
  {
    index=abs(addr[lo]-number);
    if (index<kolik_min)
    {
      kolik_min=index;
      kde_min=lo;
    }
  }
  kam[i]=kde_min;
  std::cout << "kam[" << i << "]=" << kde_min << std::endl;
  i++;
  }
  //kam[n]=N;
  return i-1;
}

int najdi_hranice2(ullong N,ullong podil,uint n,uint *addr,uint *kam) 
{
  // boundaries for small regions
  unsigned int i,lo,hi,kde_min,pocet,old;
  ullong number,kolik_min,index;
  
  kam[0]=0;
  i=1;
  //uint podil=N/(threads);
  while(kde_min<n)
  {
  old=addr[kam[i-1]];

  number=std::max(i*podil,old+podil);
  if (i==0)
  {  lo=0; //hi=bs; if (hi>n) hi=n;}
     hi=n; }
  else
  {  lo=kam[i-1]+1; //hi=kam[i-1]+bs; if (hi>n)  
  hi=n;}
  while ((hi-lo)>5)
  {
    index=(hi+lo)/2;
    //std::cout << "index=" << index << " a[]=" << addr[index] << " num=" << number << std::endl;
    if (addr[index]>number) hi=index;
    else lo=index;
  }
  
  kolik_min=abs(addr[lo]-number);
  kde_min=lo;
  lo++;
  for(;lo<=hi;lo++)
  {
    index=abs(addr[lo]-number);
    if (index<=kolik_min)
    {
      kolik_min=index;
      kde_min=lo;
    }
  }
  if (abs(number-N)<(10*threads)) kde_min=n;
  kam[i]=kde_min;
  //std::cout << "kam[" << i << "]=" << kde_min << std::endl;
  i++;
  } // end of while
  for(lo=0;lo<i;lo++)
    std::cout << "kam[" << lo << "]=" << kam[lo] << std::endl;

  //kam[n]=N;
  return i-1;
}


int najdi_hranice16(uint kolik,ullong N,uint n,ullong *addr,uint *kam) 
{
  // boundaries for regions (2^16)
  unsigned int i,lo,hi,kde_min,pocet;
  ullong number,kolik_min,index;
  
  kam[0]=0;
  hi=i=0;
  do{
    i++;
    hi+=1<<16;
    if (hi>n) hi=n;
    kam[i]=hi;
  }while (hi<n);
  return i;
}

//pocty(pole_csr, pole_x, mm,hranice_x,bnox,hranice_x,bnoy);  
void pocty(ullong *adr, uint *ci, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty) {
  // for float   Ax->y
  ullong bi,bj,i, j, ii, lj, uj;
  uint k;

  for(bi=0;bi<by;bi++)
  {
  lj=adr[hr_y[bi]];
  for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
    uj = adr[i + 1];
    for (j = adr[i]; j < adr[i + 1]; j++) {
      ii = ci[j];
      //if (ii<i) printf("1: %i %i\n",ii,i);
      for(k=0;k<bx;k++)
        if ((ii>=hr_x[k])&&(ii<hr_x[k+1]))
        {
          b_pocty[bi*bx+k]++;
          break;
        }
      //s += a[j] * x[ii];
    }
    lj = uj;
  }
  }
}


void pocty2(uint *adr, usint *ci, uint n,uint *hr_x,uint bx,uint *hr_y,uint by,uint *b_pocty) {
  // for smaller regions
  uint bi,bj,i, j, ii, lj, uj;
  uint k,k2;

  k2=0;
  for(bi=0;bi<by;bi++)
  {
  //lj=adr[hr_y[bi]];
  //std::cout << "1: " << bi << " " << hr_y[bi] <<" " << hr_y[bi+1] << std::endl;
  for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
    //uj = adr[i + 1];
    //std::cout << "2: " << i << " " << adr[i] <<" " << adr[i+1] << std::endl;
    for (j = adr[i]; j < adr[i + 1]; j++) {
      ii = ci[j];
      //if (ii<i) printf("1: %i %i\n",ii,i);
      //ii>>=16;
      //printf("1: %i %i %i\n",bi,i,j);
      //std::cout << "3: " << bi << " " << i <<" " <<j << " " <<ii<< std::endl;
      for(k=0;k<bx;k++)
        if ((ii>=hr_x[k])&&(ii<hr_x[k+1]))
        {
          b_pocty[bi*bx+k]++;
          k2++;
          break;
        } 
       //b_pocty[bi*bx+k]++; 
       
      //s += a[j] * x[ii];
    }
    //lj = uj;
  }
  }
  std::cout << "k2= " << k2<< std::endl;
}


void pocty16(ullong *adr, uint *ci, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty) {
  // for regions 2^16
  ullong bi,bj,i, j, ii, lj, uj;
  uint k;

  for(bi=0;bi<by;bi++)
  {
  lj=adr[hr_y[bi]];
  for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
    uj = adr[i + 1];
    for (j = adr[i]; j < adr[i + 1]; j++) {
      ii = ci[j];
      //if (ii<i) printf("1: %i %i\n",ii,i);
      ii>>=16;
      /*
      for(k=0;k<bx;k++)
        if ((ii>=hr_x[k])&&(ii<hr_x[k+1]))
        {
          b_pocty[bi*bx+k]++;
          break;
        } */
       b_pocty[bi*bx+ii]++; 
      //s += a[j] * x[ii];
    }
    lj = uj;
  }
  }
}



//transform(pole_a, pole_csr, pole_x, mm,hranice_x,bnox,hranice_x,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
void transform(float *pole_a,ullong *adr, uint *ci, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty,float *pole_ab,ullong **adrb, uint *cib) {
  // input CSR:
  //         pole_a,adr,ci,n
  //       hranice:
  //         hr_x,bx,hr_y,by
  //       pocty:
  //         b_pocty
  // output: block CSR
  //         pole_ab,adrb, cib
  ullong bi,bj,i,i2, j, ii, lj, uj,t;
  uint k;
  ullong old[MAX_BLO],*ladrb[MAX_BLO];
  ullong *zac_pocty = (ullong *)malloc(bx * by * sizeof(ullong));
  if (zac_pocty == NULL) {
    printf("Allocation error!!");
    return ;
  }
    
   i=0;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      ii=b_pocty[bi*bx+bj];
      zac_pocty[bi*bx+bj]=i;
      i+=ii;
    }
  }
  for(bi=0;bi<by;bi++)
  {
  // pres bloky v y
  lj=adr[hr_y[bi]];
  for(i=0;i<bx;i++) old[i]=0;
  for(i=0;i<bx;i++) 
  {
    ii=bi*bx+i;
    ladrb[i]=adrb[ii];
    if (b_pocty[ii]==0) continue; 
    ladrb[i][0]=0;
    ladrb[i][1]=0;
    old[i]=0;
  }
  for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
    i2=i-hr_y[bi];
  // over rows
    uj = adr[i + 1];
    for (j = adr[i]; j < adr[i + 1]; j++) {
    // in a row
      ii = ci[j];
      //if (ii<i) printf("2: %i %i\n",ii,i);
      for(k=0;k<bx;k++)
        if ((ii>=hr_x[k])&&(ii<hr_x[k+1]))
        {
          t=zac_pocty[bi*bx+k];
          cib[t]=ii;
          pole_ab[t]=pole_a[j];
          zac_pocty[bi*bx+k]++;
          if (i2==old[k]) 
          {
             ladrb[k][i2+1]++;
          }
          else
          {
            for(t=old[k]+2;t<=i2;t++)
              ladrb[k][t]=ladrb[k][old[k]+1];
            ladrb[k][i2+1]=ladrb[k][old[k]+1]+1;   
            old[k]=i2;
          }
          break;
        }
      //s += a[j] * x[ii];
    }
    lj = uj;
  }
  i2=hr_y[bi+1]-hr_y[bi];
  for(i=0;i<bx;i++) 
  {
    /*std::cout <<"bi1=" << bi <<" i=" << i ;
    printf("\n");   */
    ii=bi*bx+i;
    if (b_pocty[ii]==0) continue; 
    /*std::cout <<"bi2=" << bi <<" i=" << i << " old=" << old[i];
    printf("\n");  */ 
    if (old[i]<(i2-1)){
      /*std::cout <<"bi=" << bi <<" i=" << i << "old=" << old[i];
      printf("\n");  */
      for(t=old[i]+2;t<=i2;t++)
        ladrb[i][t]=ladrb[i][old[i]+1];
    }     
  }  
  }
  free(zac_pocty);
} 


void transform2(float *pole_a,uint *adr, usint *ci, uint n,uint *hr_x,uint bx,uint *hr_y,uint by,uint *b_pocty,float *pole_ab,void **adrb, usint *cib) {
  // input CSR:
  //         pole_a,adr,ci,n
  //       hranice:
  //         hr_x,bx,hr_y,by
  //       pocty:
  //         b_pocty
  // output: block CSR
  //         pole_ab,adrb, cib
  
  // pro male bloky
  ullong bi,bj,i,i2, j, ii, lj, uj,t;
  uint k,oldb;
  ullong old[MAX_BLO];
  uint *ladrb0[MAX_BLO];
  usint *ladrb1[MAX_BLO];
  int typy[MAX_BLO];  
  ullong *zac_pocty = (ullong *)malloc(bx * by * sizeof(ullong));
  if (zac_pocty == NULL) {
    printf("Allocation error!!");
    return ;
  }
    
   i=0;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      ii=b_pocty[bi*bx+bj];
      zac_pocty[bi*bx+bj]=i;
      i+=ii;
    }
  }
  printf("zac\n");
  fflush(stdout); 
  for (i = 0; i < (bx * by); i++)
  {
    std::cout << zac_pocty[i] << "  ";
    if ((i%bx)==(bx-1)) printf("\n");
  }  
  printf("pocty\n");
  fflush(stdout); 
  for (i = 0; i < (bx * by); i++)
  {
    std::cout << b_pocty[i] << "  ";
    if ((i%bx)==(bx-1)) printf("\n");
  }    
  printf("tr2: pred bl ");
  fflush(stdout);
  
  oldb=no_blocks;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      i=bi*bx+bj;
      ii=b_pocty[i];
      if (ii>0)
      {
        /*std::cout << " bl= " << no_blocks << std::endl;;
        fflush(stdout); */
        #ifdef DEBUG1  
        std::cout << " ii= " << ii <<std::endl;;   
        #endif
        pole_pol[no_blocks].startx=hr_x[bj];
        /*std::cout << " bl1= " << no_blocks << std::endl;;
        fflush(stdout); */         
        pole_pol[no_blocks].starty=hr_y[bi];
        pole_pol[no_blocks].sizex=hr_x[bj+1]-hr_x[bj];
        pole_pol[no_blocks].sizey=hr_y[bi+1]-hr_y[bi];
        #ifdef DEBUG1  
        std::cout << " bl. " << no_blocks << " sx= " << pole_pol[no_blocks].startx << " sy= " << pole_pol[no_blocks].starty;
        std::cout << " six= " << pole_pol[no_blocks].sizex << " siy= " << pole_pol[no_blocks].sizey;
        fflush(stdout);              
        #endif
        pole_pol[no_blocks].x=cib+zac_pocty[i];
        pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        pole_pol[no_blocks].temp=adrb[i];
        pole_pol[no_blocks].pocty=ii;
        #ifdef DEBUG1    
        std::cout << " p= " << pole_pol[no_blocks].pocty <<std::endl;;
        #endif
        no_blocks++;
      }
    } 
  }
  printf("tr2: po bl\n");
  fflush(stdout);
  

  for(bi=0;bi<by;bi++)
  {
  // over region in y
  #ifdef DEBUG2
     std::cout << " tr2: bi= " << bi << std::endl;;
        fflush(stdout);  
  #endif        
  lj=adr[hr_y[bi]];
  for(i=0;i<bx;i++) old[i]=0;
  for(i=0;i<bx;i++) 
  {
    /* std::cout << "1a: i= " << i << std::endl;
        fflush(stdout);    */ 
    ii=bi*bx+i;
    ladrb0[i]=(uint *)adrb[ii];
    ladrb1[i]=(usint *)adrb[ii];
    /*    std::cout << "1b: i= " << i << std::endl;
        fflush(stdout);     */
    if (b_pocty[ii]==0) continue; 
    k=hr_y[bi+1]-hr_y[bi];        
        if (b_pocty[ii]>k)
          typy[i]=0;
        else        
          typy[i]=1;    
    //if (b_pocty[bi*bx+i]>(1<<16))
    #ifdef DEBUG2
     std::cout << " tr2: typy[" << i << "]= " << typy[i] << std::endl;
    #endif 
     //   fflush(stdout);
    if (typy[i]==0)
    {
       /* std::cout << "1c: i= " << i << std::endl;
        fflush(stdout);  */   
    ladrb0[i][0]=0;
    ladrb0[i][1]=0;
    }
       /* std::cout << "1d: i= " << i << std::endl;
        fflush(stdout);  */   
    old[i]=0;
  }
  for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
      /*std::cout << "2: i= " << i << std::endl;;
        fflush(stdout);   */  
        /*if ((bi==5)&&(i>=000))
        {
        std::cout << " i= " << i << " od= " << adr[i] << " do= " << adr[i+1] <<std::endl;;
        fflush(stdout);    
        } */    
    i2=i-hr_y[bi];
  // pres radky
    uj = adr[i + 1];                           
    for (j = adr[i]; j < adr[i + 1]; j++) {
    // v radku
      ii = ci[j];
        /*if ((bi==5)&&(i>47200))
        {
        std::cout << " i= " << i << " i2= " << i2 << " j= " << j<< " ci= " << ii << std::endl;;
        fflush(stdout);    } */      
      //if (ii<i) printf("2: %i %i\n",ii,i);
      for(k=0;k<bx;k++)
        if ((ii>=hr_x[k])&&(ii<hr_x[k+1]))
        {
        /*if ((bi==5)&&(i>47200))
        {
        std::cout << " k= " << k << " pocty= " << b_pocty[bi*bx+k] << " old= " << old[k] << " typ= " << typy[k] <<std::endl;;
        fflush(stdout);    
        } */        
        
        //if (b_pocty[bi*bx+k]>(1<<16))
          t=zac_pocty[bi*bx+k];
          cib[t]=ii-hr_x[k];//ii;
          pole_ab[t]=pole_a[j];
          zac_pocty[bi*bx+k]++;
                  
        if (typy[k]==0)
        {

          if (i2==old[k]) 
          {
             ladrb0[k][i2+1]++;
          }
          else
          {
            for(t=old[k]+2;t<=i2;t++)
              ladrb0[k][t]=ladrb0[k][old[k]+1];
            ladrb0[k][i2+1]=ladrb0[k][old[k]+1]+1;   
            old[k]=i2;
          }
        }
        else
        {
          /*if ((bi==5)&&(i>47200))
          {
           std::cout << " COO start " << old[k] << std::endl;;
           fflush(stdout);    
          } */
          ladrb1[k][old[k]]=i2;
          old[k]++;
          /*if ((bi==7)&&(i>58400))
          {
           std::cout << " COO end " << old[k] << std::endl;;
           fflush(stdout);    
          } */

        }
        break;
       }        
       
            
      //s += a[j] * x[ii];
    }
    lj = uj;
  }
  i2=hr_y[bi+1]-hr_y[bi];
  for(i=0;i<bx;i++) 
  {
    /*std::cout <<"bi1=" << bi <<" i=" << i ;
    printf("\n");   */
    ii=bi*bx+i;
    if (b_pocty[ii]==0) continue; 
    
    //if (b_pocty[ii]>(1<<16))
    if (typy[i]==0)
    {    
    /*std::cout <<"bi2=" << bi <<" i=" << i << " old=" << old[i];
    printf("\n");  */ 
      if (old[i]<(i2-1)){
      /*std::cout <<"bi=" << bi <<" i=" << i << "old=" << old[i];
      printf("\n");  */
      for(t=old[i]+2;t<=i2;t++)
        ladrb0[i][t]=ladrb0[i][old[i]+1];
    }     
    }
  }  
  }
  printf("tr2: after tr\n");
  fflush(stdout);  
  
  free(zac_pocty);
} 



void transform16(float *pole_a,ullong *adr, uint *ci, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty,float *pole_ab,void **adrb, usint *cib) {
  // input CSR:
  //         pole_a,adr,ci,n
  //       hranice:
  //         hr_x,bx,hr_y,by
  //       pocty:
  //         b_pocty
  // output: block CSR  (2^16)
  //         pole_ab,adrb, cib
  
  // for 2^16 regions
  ullong bi,bj,i,i2, j, ii, lj, uj,t;
  uint k,ix,iy;
  ullong old[MAX_BLO];
  uint *ladrb0[MAX_BLO];
  usint *ladrb1[MAX_BLO];
  int typy[MAX_BLO]; 
  ullong *zac_pocty = (ullong *)malloc(bx * by * sizeof(ullong));
  if (zac_pocty == NULL) {
    printf("Allocation error!!");
    return ;
  }
    
   i=0;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      ii=b_pocty[bi*bx+bj];
      zac_pocty[bi*bx+bj]=i;
      i+=ii;
    }
  }
  
  printf("pred bl\n");
  std::cout << " i= " << i << std::endl;;
  fflush(stdout);
  
  no_blocks=0;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      i=bi*bx+bj;
      ii=b_pocty[i];
      if (ii>0)
      {
        /*std::cout << " bl= " << no_blocks << std::endl;;
        fflush(stdout); */     
        ix=pole_pol[no_blocks].startx=bj*(1<<16);
        /*std::cout << " bl1= " << no_blocks << std::endl;;
        fflush(stdout); */         
        iy=pole_pol[no_blocks].starty=bi*(1<<16);
        k=1<<16;
        if ((k+ix)>n) k=n-ix;
        pole_pol[no_blocks].sizex=k;
        k=1<<16;
        if ((k+iy)>n) k=n-iy;        
        pole_pol[no_blocks].sizey=k;

        /*std::cout << " bl2= " << no_blocks << std::endl;;
        fflush(stdout);*/              
        pole_pol[no_blocks].x=cib+zac_pocty[i];
        pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        pole_pol[no_blocks].temp=adrb[i];
        pole_pol[no_blocks].pocty=ii;
        no_blocks++;
      }
    } 
  }
  printf("po bl\n");
  fflush(stdout);
  

  for(bi=0;bi<by;bi++)
  {
  // over regions in y
        /*std::cout << " bi= " << bi << std::endl;;
        fflush(stdout);  */
  lj=adr[hr_y[bi]];
  for(i=0;i<bx;i++) old[i]=0;
  for(i=0;i<bx;i++) 
  {
        /*std::cout << " i= " << i << std::endl;;
        fflush(stdout);   */  
    ii=bi*bx+i;
    ladrb0[i]=(uint *)adrb[ii];
    ladrb1[i]=(usint *)adrb[ii];
    if (b_pocty[ii]==0) continue; 
    k=(1<<16);
    if ((k+bi*(1<<16))>n) k=n-bi*(1<<16);        
        if (b_pocty[ii]>k)
          typy[i]=0;
        else        
          typy[i]=1;
     //std::cout << " tr16: typy[" << i << "]= " << typy[i] << std::endl;;      
    if (typy[i]==0)
    {
    ladrb0[i][0]=0;
    ladrb0[i][1]=0;
    }
    old[i]=0;
  }
  for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
   /*     if (i>65500)
        {
        std::cout << " i2= " << i << " od= " << adr[i] << " do= " << adr[i+1] <<std::endl;;
        fflush(stdout);    
        }     */
    i2=i-hr_y[bi];
  // over rows
    uj = adr[i + 1];                           
    for (j = adr[i]; j < adr[i + 1]; j++) {
    // in a row
      /*  if (i>65500)
        {
        std::cout << " i2= " << i << " j= " << j<< std::endl;;
        fflush(stdout);    
        } */
      ii = ci[j];
      //if (ii<i) printf("2: %i %i\n",ii,i);
      k=ii>>16;
      //if (b_pocty[bi*bx+k]>(1<<16))
          t=zac_pocty[bi*bx+k];
          cib[t]=ii&0xffff;
          pole_ab[t]=pole_a[j];
          zac_pocty[bi*bx+k]++;
      if (typy[k]==0)
      {

          if (i2==old[k]) 
          {
             ladrb0[k][i2+1]++;
          }
          else
          {
            for(t=old[k]+2;t<=i2;t++)
              ladrb0[k][t]=ladrb0[k][old[k]+1];
            ladrb0[k][i2+1]=ladrb0[k][old[k]+1]+1;   
            old[k]=i2;
          }
       }
       else
       {
          ladrb1[k][old[k]]=i2;
          old[k]++;
       }
            
      //s += a[j] * x[ii];
    }
    lj = uj;
  }
  i2=hr_y[bi+1]-hr_y[bi];
  for(i=0;i<bx;i++) 
  {
    /*std::cout <<"bi1=" << bi <<" i=" << i ;
    printf("\n");   */
    ii=bi*bx+i;
    if (b_pocty[ii]==0) continue; 
    //if (b_pocty[ii]>(1<<16))
    if (typy[i]==0)
    {    
    /*std::cout <<"bi2=" << bi <<" i=" << i << " old=" << old[i];
    printf("\n");  */ 
      if (old[i]<(i2-1)){
      /*std::cout <<"bi=" << bi <<" i=" << i << "old=" << old[i];
      printf("\n");  */
      for(t=old[i]+2;t<=i2;t++)
        ladrb0[i][t]=ladrb0[i][old[i]+1];
    }     
    }
  }  
  }
  free(zac_pocty);
} 


void rozdel(ullong podil,int ktery)
{
// normalization of regions
/* struct polozka {
  uint xs, ys;
  uint sx,sy;
  float *a; // hodnoty a
  usint *x; // hodnoty x
  void *temp; // bud CSR (addr) nebo COO(Y)
  ullong pocty,kod;
};*/
  uint hranice_x[MAX_BLO];
  uint hranice_y[MAX_BLO];
  uint i,j,ii,jj,bnox,bnoy,mm,zz,oldno,sx,sy,k; 
  uint *adr;
  
  std::cout << "Rozdel: " << ktery << std::endl;
  fflush(stdout);
  
  adr=(uint *)pole_pol[ktery].temp;
  
  /* for(i=0;i<10;i++)
    std::cout << "2: " << i << " " << adr[i] <<" " << adr[i+1] << std::endl;
  */
  mm=pole_pol[ktery].sizex;
  uint *pole_csc = (uint *)malloc((mm+1) * sizeof(uint));
  if (pole_csc == NULL) {
    printf("Allocation error!!");
    return;
  }

    
  //pole_csr[mm]  
  do_csc16(pole_pol[ktery].x,mm,pole_pol[ktery].pocty,pole_csc);

  /* for(i=0;i<10;i++)
    std::cout << "2: " << i << " " << adr[i] <<" " << adr[i+1] << std::endl;
  */
 
  bnox=najdi_hranice2(pole_pol[ktery].pocty,podil,pole_pol[ktery].sizex,pole_csc,hranice_x);
  //(ullong N,uint n,uint *addr,uint *kam) 
  
  printf("Rozdel: bnox=%i\n",bnox);
  fflush(stdout);
  bnoy=najdi_hranice2(pole_pol[ktery].pocty,podil,pole_pol[ktery].sizey,(uint *)pole_pol[ktery].temp,hranice_y);
  //bnoy=najdi_hranice16(b_no,pole_csr[mm],mm,pole_csr,hranice_y); 
  printf("Rozdel: bnoy=%i\n",bnoy);
  fflush(stdout);
  /*for(i=0;i<10;i++)
    std::cout << "2: " << i << " " << adr[i] <<" " << adr[i+1] << std::endl;
  */

  uint *b_pocty = (uint *)malloc(bnox * bnoy * sizeof(uint));
  if (b_pocty == NULL) {
    printf("Allocation error!!");
    return;
  }
  for (i = 0; i < (bnox * bnoy); i++)
    b_pocty[i] = 0;  
  pocty2((uint *)pole_pol[ktery].temp, pole_pol[ktery].x, pole_pol[ktery].sizex,hranice_x,bnox,hranice_y,bnoy,b_pocty);    
  //pocty2(uint *adr, usint *ci, uint n,uint *hr_x,uint bx,uint *hr_y,uint by,uint *b_pocty)
  j=0;
  for (i = 0; i < (bnox * bnoy); i++)
  {
    std::cout << b_pocty[i] << "  ";
    j+= b_pocty[i];
    if ((i%bnox)==(bnox-1)) printf("\n");
  }  
  std::cout << "sum: " << j << std::endl;
  
  void ** pole_csrb = (void **)malloc((bnox * bnoy) * sizeof(void *));
  if (pole_csrb == NULL) {
    printf("Allocation error!!");
    return;
  }  
  jj=0;
  for (i = 0; i < bnoy; i++)
  {
    for (j = 0; j < bnox; j++)
    {
       ii=i*bnox+j;
      if (b_pocty[ii]>0)
      {
        jj++;
        k=hranice_y[i+1]-hranice_y[i];
        //if ((k+bi*(1<<16))>n) k=n-bi*(1<<16);        
        
        //if (b_pocty[ii]>(1<<16))
        if (b_pocty[ii]>k)
          pole_csrb[ii] = (void *)malloc((1+hranice_y[i+1]-hranice_y[i]) * sizeof(uint));
        else
          pole_csrb[ii] = (void *)malloc(b_pocty[ii] * sizeof(usint));    
        if (pole_csrb[ii] == NULL) {
          printf("Allocation error!!");
        return;
      } }    
      else
        pole_csrb[ii] = NULL;
  }}
  
  std::cout << "Rozdel: new nenul bloku= " << jj << std::endl;  
  
  zz=pole_pol[ktery].pocty;
   float *pole_ab = (float *)malloc((zz) * sizeof(float));
  if (pole_ab == NULL) {
    printf("Allocation error!!");
    return;
  } 
  // TODO pro DENSE nutno predelat
  
  usint *pole_xb = (usint *)malloc(zz * sizeof(usint));
  if (pole_xb == NULL) {
    printf("Allocation error!!");
    return;
  }  
  //pole_a = pole_pol[ktery].a
  //pole_csr = pole_pol[ktery].temp
  oldno=no_blocks;

  transform2(pole_pol[ktery].a, (uint *)pole_pol[ktery].temp, pole_pol[ktery].x, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb);
  
  sx=pole_pol[ktery].startx;
  sy=pole_pol[ktery].starty;
  for(i=oldno;i<no_blocks;i++)
  {
    pole_pol[i].startx+=sx;
    pole_pol[i].starty+=sy;
  }
  #ifdef DEBUG5
  for(i=0;i<no_blocks;i++)
      {
        std::cout << "Rozdel: Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;;
      }
  #endif

  pole_pol[ktery]=pole_pol[no_blocks-1];
  no_blocks--;
    
}

#include "csr.cpp"

#include "coo.cpp"

#include "mv.cpp"


void boundary()
{
  uint b,b2,b3,x,y,rx,ry,i,j,k;
  uint boundsx[MAX_BLO];
  uint boundsy[MAX_BLO];
  ullong tt[MAX_BLO],t,t2,t3;
  ullong mx[MAX_BLO],maxx,maxy;
  ullong my[MAX_BLO];
  
/* struct polozka {
  uint xs, ys;
  uint sx,sy;
  float *a; // hodnoty a
  usint *x; // hodnoty x
  void *temp; // bud CSR (addr) nebo COO(Y)
  ullong pocty,kod;
};*/
  rx=ry=0;
  for (i = 0; i < no_blocks; i++) {
    x=pole_pol[i].startx;
    k=1;
    for(j=0;j<rx;j++)
      if (boundsx[j]==x) {k=0; break;}
    if (k) 
    {
      boundsx[rx]=x;
      rx++;
    } 
    x=pole_pol[i].startx+pole_pol[i].sizex;
    k=1;
    for(j=0;j<rx;j++)
      if (boundsx[j]==x) {k=0; break;}
    if (k) 
    {
      boundsx[rx]=x;
      rx++;
    } 
    y=pole_pol[i].starty;
    k=1;
    for(j=0;j<ry;j++)
      if (boundsy[j]==y) {k=0; break;}
    if (k) 
    {
      boundsy[ry]=y;
      ry++;
    } 
    y=pole_pol[i].starty+pole_pol[i].sizey;
    k=1;
    for(j=0;j<ry;j++)
      if (boundsy[j]==y) {k=0; break;}
    if (k) 
    {
      boundsy[ry]=y;
      ry++;
    } 

  }
  qsort(boundsx, rx, sizeof(uint), compare_uint);
  qsort(boundsy, ry, sizeof(uint), compare_uint);
  std::cout << "x=";
  for(i=0;i<rx;i++)
    std::cout << " " <<  boundsx[i] ;
  std::cout  << std::endl;    
  std::cout << "y=";
  for(i=0;i<ry;i++)
    std::cout << " " <<  boundsy[i] ;
  std::cout  << std::endl;    
  
  for(i=0;i<rx;i++) mx[i]=0;
  for(i=0;i<ry;i++) my[i]=0;

  for (i = 0; i < no_blocks; i++) {
    x=pole_pol[i].startx;
    k=1;
    for(j=0;j<rx;j++)
      if (boundsx[j]==x) {k=0; break;}
    pole_pol[i].mx1=j;
    x=pole_pol[i].startx+pole_pol[i].sizex;
    k=1;
    for(j=0;j<rx;j++)
      if (boundsx[j]==x) {k=0; break;}
    pole_pol[i].mx2=j;
    y=pole_pol[i].starty;
    k=1;
    for(j=0;j<ry;j++)
      if (boundsy[j]==y) {k=0; break;}
    pole_pol[i].my1=j;
    y=pole_pol[i].starty+pole_pol[i].sizey;
    k=1;
    for(j=0;j<ry;j++)
      if (boundsy[j]==y) {k=0; break;}
    pole_pol[i].my2=j;
    }
   std::cout << "boundaries found" << std::endl;
   fflush(stdout);    
   noboundsx=rx;
   noboundsy=ry;
}   
 
     
void plan_fussed()
{
  // static planning for fussed op.
  uint b,b2,b3,x,y,rx,ry,i,j,k;
  uint boundsx[MAX_BLO];
  uint boundsy[MAX_BLO];
  ullong tt[MAX_BLO],t,t2,t3;
  ullong mx[MAX_BLO],maxx,maxy;
  ullong my[MAX_BLO];
  uint p_reg[MAX_BLO];
  uint p_th[MAX_BLO];
  uint indexy[MAX_THR];  
     
   for(i=0;i<no_blocks;i++) {   
   pole_exec[i].done=0;
   mx[i]=my[i]=0;
   }
   for(i=0;i<threads;i++) tt[i]=0;
   j=0;
   
   while(j<no_blocks)
   {
     t=tt[0];
     k=0;
     for(i=1;i<threads;i++)
       if (tt[i]<t) {t=tt[i]; k=i;}
       
     //std::cout << "t=" << t << "k=" << k <<std::endl;  
     // k = thread;     
     // t = current time
     //std::cout << "time=" << t << " thr=" << k <<std::endl;
     t3=1l<<31l;
     for(b=0;b<no_blocks;b++)
     {
       if (pole_exec[b].done>0) continue;
       maxx=maxy=0;
       for(b2=pole_pol[b].mx1;b2<pole_pol[b].mx2;b2++)
         if (maxx<mx[b2]) maxx=mx[b2];
       for(b2=pole_pol[b].my1;b2<pole_pol[b].my2;b2++)
         if (maxy<my[b2]) maxy=my[b2];
       
       if (maxx<maxy) maxx=maxy;
       if (t3>maxx) { t3=maxx; b3=b;} 
     }
     // t3,b3 choosen region
    //std::cout << "time=" << t3 << " th=" << k << " b3=" << b3  <<std::endl;     
    //std::cout << "time=" << t3 << " bl=" << b3 << " : "<< maxx << "," << maxy << std::endl;     

      /*if ((t<=maxx) && (t<=maxy))    
      { */
    /*std::cout << "thr=" << k << " bl=" << b3 << " tt=" << tt[k] <<std::endl;
    fflush(stdout);     */
    pole_exec[b3].done=1;
    t2=t;
    if (t2<t3) t2=t3; // volny thread nebo mutexy 
    t2+=pole_pol[b3].pocty;
    tt[k]=t2;
    //std::cout << "t2=" << t2 <<std::endl;
    //pole_exec[b3].time=t2;
    p_th[j]=k;
    p_reg[j]=b3;
    for(b2=pole_pol[b3].mx1;b2<pole_pol[b3].mx2;b2++)
      mx[b2]=t2;
    for(b2=pole_pol[b3].my1;b2<pole_pol[b3].my2;b2++)
      my[b2]=t2;        
    j++;     
   }
   for(i=0;i<threads;i++)
     std::cout << "thr=" << i << " time=" << tt[i] <<std::endl;
 for(i=0;i<threads;i++)
 {
   plan_thr_num[i]=0;
 }
 for(i=0;i<no_blocks;i++)  
 {
   //std::cout << " l="<< l << " t (" << i << ")= "; 
   plan_thr_num[p_th[i]]++;
  }
  uint temp;
  uint l=0;
  for(i=0;i<threads;i++)
  {
    temp=plan_thr_num[i];
    indexy[i]=plan_thr_num[i]=l;
    l+=temp;
  }
  plan_thr_num[threads]=no_blocks;
   
  for(i=0;i<no_blocks;i++)
  {
     plan_thr_reg[indexy[p_th[i]]++]=p_reg[i]; 
     //std::cout << pole_reg[i][k2] << " "; 
   }
   //std::cout <<std::endl; 
  /* 
  for(i=0;i<=threads;i++)
  {     
     std::cout << i << " = " << plan_thr_num[i] << std::endl;
   }   
   
  for(i=0;i<no_blocks;i++)
  {
     std::cout << i << " = " << plan_thr_reg[i] << std::endl; 
   }*/          
}    


void kalibr_cas()
{  double start,end;
          start=omp_get_wtime();
          end=omp_get_wtime();
          std::cout << " kalibr: " << end-start << std::endl;
}


void check_res(float *pole_yf, float *pole_yf2, float *pole_yk1, float *pole_yk2, uint mm)
{
  // to check correctness of the result
  uint i;
  float x;
  for(i=0;i<mm;i++)
  {
    x=fabs(pole_yf[i]-pole_yk1[i]);
    if (x>0.1) 
      std::cout << "i1=" << i << " " << pole_yf[i] << " " << pole_yk1[i] <<std::endl;
    x=fabs(pole_yf2[i]-pole_yk2[i]);
    if (x>0.1) 
      std::cout << "i2=" << i << " " << pole_yf2[i] << " " << pole_yk2[i] <<std::endl;
  
  }
}

void zkrat_CSR()
{
  // eliminate heading and tailing empty rows
  uint * ukaz;
  int i,j1,j2;
  for(i=0;i<no_blocks;i++)
      {
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
           ukaz=(uint *)pole_pol[i].temp;
           j1=1;
           while(ukaz[j1]==0) j1++;
           pole_pol[i].lower=j1-1;
           j2=pole_pol[i].sizey-1;
           while(ukaz[j2]==pole_pol[i].pocty) j2--;
           pole_pol[i].upper=j2+1;
           if ((pole_pol[i].lower!=0)||(pole_pol[i].upper!=pole_pol[i].sizey)) 
           {
             std::cout << "blok " << i << " j1=" << j1 << " j2=" << j2 << std::endl;         
             std::cout << " sy=" <<pole_pol[i].sizey << " up=" << pole_pol[i].upper << " low =" << pole_pol[i].lower << std::endl;
        } }
      }
}

int main(int argc, char *argv[]) {
  ullong Z; // quad size
  uint bnox,bnoy;
  ullong i, j, ii, jj, nops;
  float dd;
  double d;
  uint k; 
  uint hranice_x[MAX_BLO];
  uint hranice_y[MAX_BLO];
  int soucet=0;
  
  std::cout << "Argc:" << argc << std::endl;
  if (argc!=5)
  {  
    //std::cout << "cmd line: <.exe> <filename> <nop> <block_number_in_1D> <max. block size>" << argc << std::endl;
    std::cout << "cmd line: <.exe> <filename> <nop> <no. of threads> <block thr.> argc=" << argc << std::endl;
    return -1;
  }
  std::ifstream ifs(argv[1]);
  std::cout << "filename: " << argv[1] << std::endl;
  while ('%' == ifs.peek())
    ifs.ignore(1024, '\n');
  nops = atoi(argv[2]);
  threads = atoi(argv[3]);
  block_thr = atof(argv[4]);
  /*b_no = atoi(argv[3]);
  bs = atoi(argv[4]);  */
  ifs >> mm >> nn >> zz;
  /*poi= new int[M];
  if (poi == NULL)
    std::cout << "Error: memory could not be allocated";
  */

  std::cout << "Matrix file:              " << argv[1] << std::endl;
  std::cout << "Number of rows:           " << mm << std::endl;
  std::cout << "Number of columns:        " << nn << std::endl;
  std::cout << "Number of entries:        " << mm * nn << std::endl;
  std::cout << "Number of nonzeros:       " << zz << std::endl;

  pole_x = (uint *)alokace(zz * sizeof(int));
  pole_y = (uint *)alokace(zz * sizeof(int));
  pole_csr = (ullong *)alokace((mm + 1l) * sizeof(ullong));
  float *pole_a = alokacef(zz);//(float *)malloc((zz) * sizeof(float));

  // printf("Begin of read\n");
  fflush(stdout);
  int oi,oj;
  oi=oj=0;
  
  double temp_d;
  for (ii = 0; ii < zz; ii++) {
    // printf("ii=%li ",ii);
    // fflush(stdout);
    d=1.0;
    if (nops == 4)
      ifs >> i >> j >> d >> temp_d;    
    if (nops == 3)
      ifs >> i >> j >> d;
    if (nops == 2)
      ifs >> i >> j;
    i--;
    j--;
    if ((j<oj)||((j==oj)&&(i<oi)))
    {
       std::cout << "Wrong "  << ii << "" << std::endl;
       std::cout << i << " "  << j << " "<< oi << " "  << oj << " "<<std::endl;
    }
    if (i>=mm) i=0;
    if (j>=mm) i=0;
    pole_x[ii] = (int)i;
    pole_y[ii] = (int)j;
    pole_a[ii] = d;
    oi=i;
    oj=j;
    // std::cout << "i=" << i << " j=" <<j<<std::endl;
    // fflush(stdout);
  }
  std::cout << "Read OK "  << std::endl;
  fflush(stdout);
  
  for (ii = 0; ii < zz; ii++)
    pole_a[ii] = 1.0;
    
  
  // printf("Nacteno\n");
  // fflush(stdout);
  for (ii = 0; ii < mm; ii++) {
    pole_csr[ii] = -1l;
    // printf("ii=%li ",ii);
    // fflush(stdout);
  }
  // printf("csr0\n");
  // fflush(stdout);
  for (ii = zz - 1; ; ii--) {
    pole_csr[pole_y[ii]] = ii;
    if (ii == 0)
      break;
    // printf("ii2=%li ",ii);
    // fflush(stdout);
  }
  pole_csr[0] = 0;
  pole_csr[mm] = zz;
  
  for (ii = mm-1; ii > 0; ii--)
    if (pole_csr[ii]==-1) pole_csr[ii]=pole_csr[ii+1];
  // printf("CSR hotovo1\n");
  // fflush(stdout);
  std::cout << "CSR OK "  << std::endl;
  fflush(stdout);
  long prev = 0l;
  for (long i = 0; i < mm + 1; ++i) {
    if (pole_csr[i] < prev) {
      pole_csr[i] = prev;
    }
    prev = pole_csr[i];
  }
  // printf("CSR hotovo2\n");
  // fflush(stdout);
  /*
  for(ii=0;ii<mm;ii++)
  {
    std::cout << "CSR[" << ii << "]=" << pole_csr[ii] << std::endl;
  }
  fflush(stdout);
  */

  // Z=zikzak(0,0,0,n-1,n-1);
  // std::cout << "Number of bits for zikzak:  " << Z << std::endl;
  kalibr_cas();


  float *pole_xf = alokacef(mm);//(float *)malloc((mm) * sizeof(float));
  for (ii = 0; ii < mm; ii++)
    pole_xf[ii] = i%7-3.0;

  float *pole_yf = alokacef(mm);//(float *)malloc((mm) * sizeof(float));
  for (ii = 0; ii < mm; ii++)
    pole_yf[ii] = 0.0;

  float *pole_yk1 = alokacef(mm);//(float *)malloc((mm) * sizeof(float));
  for (ii = 0; ii < mm; ii++)
    pole_yk1[ii] = 0.0;

  float *pole_yk2 = alokacef(mm);//(float *)malloc((mm) * sizeof(float));
  for (ii = 0; ii < mm; ii++)
    pole_yk2[ii] = 0.0;
  std::cout << "Init OK "  << std::endl;
  fflush(stdout);
   fused_csr_f(pole_a, pole_xf, pole_yk1, pole_yk2, pole_csr, pole_x, mm);
   std::cout << "SEQ FUSED OK " << std::endl;
   fflush(stdout);
  ullong *pole_csc = (ullong *)alokace((mm+1) * sizeof(ullong));

  for (ii = 0; ii <= mm; ii++)
    pole_csc[ii] = 0;
    
  do_csc(pole_x,mm,pole_csr[mm],pole_csc);

  bnox=najdi_hranice16(b_no,pole_csc[mm],mm,pole_csc,hranice_x);
  printf("bnox=%i\n",bnox);
  
  bnoy=najdi_hranice16(b_no,pole_csr[mm],mm,pole_csr,hranice_y); 
  printf("bnoy=%i\n",bnoy);
  ullong *b_pocty = (ullong *)alokace(bnox * bnoy * sizeof(ullong));

  for (ii = 0; ii < (bnox * bnoy); ii++)
    b_pocty[ii] = 0;  
  pocty(pole_csr, pole_x, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty);  
    
  for (ii = 0; ii < (bnox * bnoy); ii++)
  {
    std::cout << b_pocty[ii] << "  ";
    if ((ii%bnox)==(bnox-1)) printf("\n");
  }
   
  /*    float * pole_x3=(float *)malloc((mm)*sizeof(float));
      if (pole_x3==NULL)
      {
          printf("Allocation error!!");
          return -1;
      }
      for(ii=0;ii<mm;ii++) pole_x3[ii]=1.0;
  */
   float *pole_ab = alokacef(zz);//(float *)malloc((zz) * sizeof(float));

  // TODO pro DENSE nutno predelat
  
  usint *pole_xb = (usint *)alokace(zz * sizeof(usint));
  // TODO pro DENSE nutno predelat
  
  void ** pole_csrb = (void **)alokace((bnox * bnoy) * sizeof(void *));

  jj=0;
  for (i = 0; i < bnoy; i++)
  {
    for (j = 0; j < bnox; j++)
    {
       ii=i*bnox+j;
      if (b_pocty[ii]>0)
      {
        jj++;
        k=hranice_y[i+1]-hranice_y[i];
        /*    k=(1<<16);
    if ((k+bi*(1<<16))>n) k=n-bi*(1<<16);   */     
        //if (b_pocty[ii]>(1<<16))
        if (b_pocty[ii]>k)
          pole_csrb[ii] = (void *)malloc((1+hranice_y[i+1]-hranice_y[i]) * sizeof(uint));
        else
          pole_csrb[ii] = (void *)malloc(b_pocty[ii] * sizeof(usint));    
        if (pole_csrb[ii] == NULL) {
          printf("Allocation error!!");
        return -1;
      } }    
      else
        pole_csrb[ii] = NULL;
  }}
  
  std::cout << "main: nenul bloku= " << jj << std::endl;
  
  // TODO bude to stacit ???
  //threads*threads*jj
  pole_pol=(struct polozka *)alokace(MAX_BLO * sizeof(struct polozka));
  
  printf("main: pred tr\n");
  fflush(stdout);
    
  transform16(pole_a, pole_csr, pole_x, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 


  printf("main: po tr\n");
  fflush(stdout);

  j=0;
  for(i=0;i<no_blocks;i++)
      {
        j+= pole_pol[i].pocty;
      }
  std::cout << " sum pol = "<< j  << std::endl;;
  /*
  for (i = hranice_y[0]; i < hranice_y[1]; i++)
  {
    std::cout << i << " = " << pole_csrb[0][i];
    printf("\n");
  } */
   /*
  for (i = 0; i < bnoy; i++)
  {
    for (j = 0; j < bnox; j++)
    {
       ii=i*bnox+j;
    if (b_pocty[ii]>0)
    {
     std::cout << pole_csrb[ii][hranice_y[i+1]-hranice_y[i]] << "  ";
    }    
    else
      std::cout << "0P ";
  }
  printf("\n");
  } 
  */
  //#define DEBUG3
  
  #ifdef DEBUG3
  for(i=0;i<no_blocks;i++)
      {
        std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        //print_reg(i);
      }
   #endif   
  // return 0;
  // mul_block(float *x, float *y, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty,float *pole_ab,ullong **adrb, uint *cib)  
  //mv_block(pole_xf, pole_yf, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
  ii=(float)(zz/(threads))*block_thr;
  if (ii<(1<<16)) ii=1<<16;
  std::cout << "main: Limit: " << ii << std::endl;
  fflush(stdout);  
  
  flush_cache=(int *)alokace(32*1024*1024);
  
  //rozdel(0);

  /*mv_block16(pole_xf, pole_yf, mm);
  return 0; */    
  j=no_blocks;
  for(i=0;i<j;i++)
  {
    if (pole_pol[i].pocty<ii) continue;
    
    rozdel(ii,i); 
  }
  #ifdef DEBUG3
  for(i=0;i<no_blocks;i++)
      {
        std::cout << "main: Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;;
      }
  #endif  
  j=0;
  for(i=0;i<no_blocks;i++)
      {
        j+= pole_pol[i].pocty;
      }
  std::cout << " sum pol2 = "<< j  << std::endl;;  

  #ifdef DEBUG9        
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));  
  mv_csr_f(pole_a, pole_xf, pole_yf, pole_csr, pole_x, mm);
  std::cout << "MV OK "<< std::endl;
  fflush(stdout);

   soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));  
  mv_block16(pole_xf, pole_yf, mm);
  std::cout << "MV BLOCK OK "<< std::endl;
  fflush(stdout);
  #endif
  //return 0;
  
  float *pole_yf2 = alokacef(mm);//(float *)malloc((mm) * sizeof(float));
  for (ii = 0; ii < mm; ii++)
    pole_yf2[ii] = 0.0;
    
    //#ifdef DEBUG3  
    double st_time,end_time,m_time;  
    soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));  
  st_time=omp_get_wtime();  
  fused_csr_f(pole_a, pole_xf, pole_yf, pole_yf2,pole_csr, pole_x, mm);
  end_time=omp_get_wtime();
  std::cout <<  end_time-st_time << std::endl;   
  fflush(stdout);  
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif  
  std::cout << "FUSED OK "<< std::endl;
  fflush(stdout);
    
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));  
  fused_block16(pole_xf, pole_yf, pole_yf2, mm);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSED BLOCK OK + check "<< std::endl;
  fflush(stdout);
  //#endif
  
  qsort(pole_pol, no_blocks, sizeof(struct polozka), compare_pol_lex);
  
   std::cout << "SORT1 OK "<< std::endl;
  fflush(stdout); 
  
    //#ifdef DEBUG9 
    soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));  
  mv_block16(pole_xf, pole_yf, mm);
  std::cout << "SPMV16 BLOCK (lex) OK "<< std::endl;
  fflush(stdout);
  
     soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));   
  fused_block16(pole_xf, pole_yf, pole_yf2, mm);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSS16 BLOCK (lex) OK +check"<< std::endl;
  fflush(stdout);
    //#endif
  
  qsort(pole_pol, no_blocks, sizeof(struct polozka), compare_pol_mor);
  
    std::cout << "SORT2 OK "<< std::endl;
  fflush(stdout);
  
     // #ifdef DEBUG9 
    soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));  
  mv_block16(pole_xf, pole_yf, mm);
  std::cout << "SPMV16 BLOCK (mor) OK "<< std::endl;
  fflush(stdout);  
  
    soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));  
  st_time=omp_get_wtime();

  fused_block16(pole_xf, pole_yf, pole_yf2, mm);  
  #ifdef CHECK1  
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout <<  end_time-st_time << std::endl;   

  std::cout << "FUSS16 BLOCK (mor) OK "<< std::endl;
  fflush(stdout);
    //#endif
  // TODO bude to stacit ???
  pole_exec=(struct exec *)alokace(no_blocks * sizeof(struct exec));
    
  boundary();
  std::cout << "BOUNDS OK "<< std::endl;
  fflush(stdout); 

  for(i=0;i<no_blocks;i++)
      {
        std::cout << "Block no. " << i;
         if (pole_pol[i].pocty>pole_pol[i].sizey)
           std::cout << " CSR ";
         else
           std::cout << " COO ";
        std::cout << " st_x= " << pole_pol[i].startx;
        std::cout << " st_y= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey << std::endl;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " mx= " << pole_pol[i].mx1 << "/" << pole_pol[i].mx2;
        std::cout << " my= " << pole_pol[i].my1 << "/" << pole_pol[i].my2; 
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
      }  
   
  /*plan_mv();
  std::cout << "PLAN OK "<< std::endl;
  fflush(stdout);  
  */
  /*mv_plan(pole_xf, pole_yf, mm);
  std::cout << "MV PLAN OK "<< std::endl;
  fflush(stdout); */ 
  soucet+=vyprazdni(flush_cache,32*1024*1024/4);
  mv_dyn(pole_xf, pole_yf, mm);
  std::cout << "MV DYN OK "<< std::endl;
  fflush(stdout);  
  /*
  plan_fussed();
  std::cout << "PLAN OK "<< std::endl;
  fflush(stdout);  
  
  fused_plan(pole_xf, pole_yf, pole_yf2, mm); 
  std::cout << "FUSSED PLAN OK "<< std::endl;
  fflush(stdout);  */
     std::cout << "bloku="<< no_blocks<< std::endl;
  fflush(stdout); 
  zkrat_CSR();

   std::cout << "pred FUSSED"<< std::endl;
  fflush(stdout); 
  
  //soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_dyn(pole_xf, pole_yf, pole_yf2, mm,0);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED DYN0a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_dynb(pole_xf, pole_yf, pole_yf2, mm,0);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;    
  std::cout << "FUSSED DYN0 OK "<< std::endl;
  fflush(stdout);

  //soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_dyn(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED DYN1a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_dynb(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;    
  std::cout << "FUSSED DYN1 OK "<< std::endl;
  fflush(stdout);
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_dyn(pole_xf, pole_yf, pole_yf2, mm,2);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED DYN2a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_dynb(pole_xf, pole_yf, pole_yf2, mm,2);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED DYN2 OK "<< std::endl;
  fflush(stdout);    
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_dyn(pole_xf, pole_yf, pole_yf2, mm,4);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();
  fused_dynb(pole_xf, pole_yf, pole_yf2, mm,4);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED DYN4 OK "<< std::endl;
  fflush(stdout);  

  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_dyn(pole_xf, pole_yf, pole_yf2, mm,5);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();
  fused_dynb(pole_xf, pole_yf, pole_yf2, mm,5);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
    end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl; 
  std::cout << "FUSSED DYN5 OK "<< std::endl;
  fflush(stdout);
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_dyn(pole_xf, pole_yf, pole_yf2, mm,6);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();  
  fused_dynb(pole_xf, pole_yf, pole_yf2, mm,6);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED DYN6 OK "<< std::endl;
  fflush(stdout);   
  
  st_time=omp_get_wtime();
  fused_single(pole_xf, pole_yf, pole_yf2, mm,0);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED SIN0a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_singleb(pole_xf, pole_yf, pole_yf2, mm,0);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;    
  std::cout << "FUSSED SIN0 OK "<< std::endl;
  fflush(stdout);

  //soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_single(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED SIN1a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_singleb(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;    
  std::cout << "FUSSED SIN1 OK "<< std::endl;
  fflush(stdout);
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_single(pole_xf, pole_yf, pole_yf2, mm,2);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED SIN2a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_singleb(pole_xf, pole_yf, pole_yf2, mm,2);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED SIN2 OK "<< std::endl;
  fflush(stdout);    
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_single(pole_xf, pole_yf, pole_yf2, mm,4);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();
  fused_singleb(pole_xf, pole_yf, pole_yf2, mm,4);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED SIN4 OK "<< std::endl;
  fflush(stdout);  

  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_single(pole_xf, pole_yf, pole_yf2, mm,5);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();
  fused_singleb(pole_xf, pole_yf, pole_yf2, mm,5);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
    end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl; 
  std::cout << "FUSSED SIN5 OK "<< std::endl;
  fflush(stdout);
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_single(pole_xf, pole_yf, pole_yf2, mm,6);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();  
  fused_singleb(pole_xf, pole_yf, pole_yf2, mm,6);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED SIN6 OK "<< std::endl;
  fflush(stdout);  

  st_time=omp_get_wtime();
  fused_sin2(pole_xf, pole_yf, pole_yf2, mm,0);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED SIN0a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_sin2b(pole_xf, pole_yf, pole_yf2, mm,0);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;    
  std::cout << "FUSSED SINB0 OK "<< std::endl;
  fflush(stdout);

  //soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_sin2(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED SIN1a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_sin2b(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;    
  std::cout << "FUSSED SINB1 OK "<< std::endl;
  fflush(stdout);
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_sin2(pole_xf, pole_yf, pole_yf2, mm,2);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  std::cout << "FUSSED SIN2a OK "<< std::endl;
  fflush(stdout);  
  m_time=omp_get_wtime();
  fused_sin2b(pole_xf, pole_yf, pole_yf2, mm,2);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED SINB2 OK "<< std::endl;
  fflush(stdout);    
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_sin2(pole_xf, pole_yf, pole_yf2, mm,4);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();
  fused_sin2b(pole_xf, pole_yf, pole_yf2, mm,4);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED SINB4 OK "<< std::endl;
  fflush(stdout);  

  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_sin2(pole_xf, pole_yf, pole_yf2, mm,5);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();
  fused_sin2b(pole_xf, pole_yf, pole_yf2, mm,5);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
    end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl; 
  std::cout << "FUSSED SINB5 OK "<< std::endl;
  fflush(stdout);
  
  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  fused_sin2(pole_xf, pole_yf, pole_yf2, mm,6);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();  
  fused_sin2b(pole_xf, pole_yf, pole_yf2, mm,6);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED SINB6 OK "<< std::endl;
  fflush(stdout);
  
  return 0;

  
  plan_fussed();
  std::cout << "PLAN OK "<< std::endl;
  fflush(stdout);  

  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  
  //fused_plan(pole_xf, pole_yf, pole_yf2, mm); 
  fused_dyn_plan(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();
  fused_dyn_planb(pole_xf, pole_yf, pole_yf2, mm,1);
  //fused_plan(pole_xf, pole_yf, pole_yf2, mm); 
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;   
  std::cout << "FUSSED PLAN OK "<< std::endl;
  fflush(stdout);  
  
 
  soucet+=vyprazdni(flush_cache,32*1024*1024/4);
  fused_dyn_gen(pole_xf, pole_yf, pole_yf2, mm,1);
  std::cout << "FUSSED DYN gen OK "<< std::endl;
  fflush(stdout); 

  soucet+=vyprazdni(flush_cache,32*1024*1024/4);
  st_time=omp_get_wtime();  
  fused_dyn_plan(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  m_time=omp_get_wtime();  
  //std::cout << "FUSSED DYN plan OK "<< std::endl;
  //fflush(stdout);     
  //soucet+=vyprazdni(flush_cache,32*1024*1024/4);
  fused_dyn_planb(pole_xf, pole_yf, pole_yf2, mm,1);
  #ifdef CHECK1
    check_res(pole_yf, pole_yf2, pole_yk1, pole_yk2, mm);
  #endif
  end_time=omp_get_wtime();
  std::cout << " " << m_time-st_time << " " << end_time-st_time << std::endl;    
  std::cout << "FUSSED DYN planb OK "<< std::endl;
  fflush(stdout);  
  
  
  return 0;

  soucet+=vyprazdni(flush_cache,32*1024*1024/sizeof(int));
  st_time=omp_get_wtime();
  for(i=0;i<50;i++)
  {
  fused_dyn(pole_xf, pole_yf, pole_yf2, mm,6);
//  m_time=omp_get_wtime();  
  fused_dynb(pole_xf, pole_yf, pole_yf2, mm,6);
  }
  end_time=omp_get_wtime();
  std::cout << " "  << end_time-st_time << std::endl;   
  std::cout << "100 FUSSED DYN6 OK "<< std::endl;
  fflush(stdout); 
  /*
  soucet+=vyprazdni(flush_cache,32*1024*1024/4);
  fused_dyn_gen(pole_xf, pole_yf, pole_yf2, mm,5);
  std::cout << "FUSSED DYN6 gen OK "<< std::endl;
  fflush(stdout); 
  */
  /*
  soucet+=vyprazdni(flush_cache,32*1024*1024/4);
  fused_dyn_plan(pole_xf, pole_yf, pole_yf2, mm,0);
  std::cout << "FUSSED DYN6 plan OK "<< std::endl;
  fflush(stdout);

  soucet+=vyprazdni(flush_cache,32*1024*1024/4);
  fused_dyn_planb(pole_xf, pole_yf, pole_yf2, mm,0);
  std::cout << "FUSSED DYN6 planb OK "<< std::endl;
  fflush(stdout);
  */
  
  printf("\n Soucet=%i\n",soucet);
  //mv_many(pole_xf, pole_yf, mm);   
  return 0;  

  std::cout << "Cache csr spmv: ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
    mv_csr_f_cache(pole_a, pole_xf, pole_yf, pole_csr, pole_x, mm);
    cache_print();
    cache_delete();
  }
  printf("\n");

  std::cout << "Cache csr fused: ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
    fused_csr_f_cache(pole_a, pole_xf, pole_yf, pole_yf2, pole_csr, pole_x, mm);
    cache_print();
    cache_delete();
  }
  printf("\n");

  std::cout << "Cache block spmv: ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
     //mv_block_cache(pole_xf, pole_yf, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
     //
     mv_block_cache16(pole_xf, pole_yf, mm);
    cache_print();
    cache_delete();
  }
  printf("\n");

  std::cout << "Cache block fused: ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
     //fussed_block_cache(pole_xf, pole_yf, pole_yf2, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
     fused_block_cache16(pole_xf, pole_yf, pole_yf2, mm);

    cache_print();
    cache_delete();
  }
  printf("\n");

  qsort(pole_pol, no_blocks, sizeof(struct polozka), compare_pol_lex);

  std::cout << "Cache block spmv (Lex): ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
     //mv_block_cache(pole_xf, pole_yf, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
     //
     mv_block_cache16(pole_xf, pole_yf, mm);
    cache_print();
    cache_delete();
  }
  printf("\n");

  std::cout << "Cache block fused (Lex): ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
     //fussed_block_cache(pole_xf, pole_yf, pole_yf2, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
     fused_block_cache16(pole_xf, pole_yf, pole_yf2, mm);

    cache_print();
    cache_delete();
  }
  printf("\n");

  
  for (ii = 0; ii < no_blocks; ii++) {
    pole_pol[ii].kod=get_morton(pole_pol[ii].startx,pole_pol[ii].starty);
  }
  
  
  qsort(pole_pol, no_blocks, sizeof(struct polozka), compare_pol_mor);

  std::cout << "Cache block spmv (Morton): ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
     //mv_block_cache(pole_xf, pole_yf, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
     //
     mv_block_cache16(pole_xf, pole_yf, mm);
    cache_print();
    cache_delete();
  }
  printf("\n");

  std::cout << "Cache block fused (Morton): ";
  for (ii = 0; ii < 4; ii++) {
    cache_init(64l, 1 << (ii * 3 + 4), 8l, 64l, 1 << (ii * 3 + 5), 8l, 64l,
               1 << (ii * 3 + 5), 8l);
     //fussed_block_cache(pole_xf, pole_yf, pole_yf2, mm,hranice_x,bnox,hranice_y,bnoy,b_pocty,pole_ab, pole_csrb, pole_xb); 
     fused_block_cache16(pole_xf, pole_yf, pole_yf2, mm);

    cache_print();
    cache_delete();
  }
  printf("\n");

  free(flush_cache);
  //free(pole_pol);
  free(pole_yf2);
  free(pole_yf);
  free(pole_xf);
  free(pole_a);
  free(pole_csr);
  free(pole_x);
  free(pole_y);
  return 0;
}