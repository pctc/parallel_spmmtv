/*
  for multiply of blocks(regions)
*/

void mv_block(float *x, float *y, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty,float *pole_ab,ullong **adrb, uint *cib) {
  // input n
  //       hranice:
  //         hr_x,bx,hr_y,by
  //       pocty:
  //         b_pocty
  // output: block CSR
  //         pole_ab,adrb, cib
  ullong bi,bj,i,i2, j, ii, lj, uj,t;
  uint k,prvni;
  ullong old[MAX_BLO],*ladrb[MAX_BLO];
  ullong *zac_pocty = (ullong *)malloc(bx * by * sizeof(ullong));
  float s;
  
  if (zac_pocty == NULL) {
    printf("Allocation Error!!");
    return ;
  }
    
   i=0;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      ii=b_pocty[bi*bx+bj];
      zac_pocty[bi*bx+bj]=i;
      i+=ii;
    }
  }
  for(bi=0;bi<by;bi++)
  {
  // pres bloky v y
  //lj=adr[hr_y[bi]];

  prvni=1;
  for(bj=0;bj<bx;bj++)
  {
    // pres bloky v y
    k=bi*bx+bj;
    if (b_pocty[k]==0) continue;
    t=zac_pocty[k];
    for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
    i2=i-hr_y[bi];
    // pres radky    
    //uj = adr[i + 1];
    s=0.0;
    for (j = adrb[k][i2]; j < adrb[k][i2 + 1]; j++) {
    // v radku
      ii = cib[j+t];
      s += pole_ab[j+t] * x[ii];
      //s += a[j] * x[ii];
    }
    if (prvni) { y[i] = s; }
    else { y[i] += s; }
    //lj = uj;
    }
    prvni=0;
   // lj = uj;
   // cache_read(y + i);
  }
  }
} 



void mv_block_cache(float *x, float *y, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty,float *pole_ab,ullong **adrb, uint *cib) {
  // input n
  //       hranice:
  //         hr_x,bx,hr_y,by
  //       pocty:
  //         b_pocty
  // output: block CSR
  //         pole_ab,adrb, cib
  ullong bi,bj,i,i2, j, ii, lj, uj,t;
  uint k,prvni;
  ullong old[MAX_BLO],*ladrb[MAX_BLO];
  ullong *zac_pocty = (ullong *)malloc(bx * by * sizeof(ullong));
  float s;
  
  if (zac_pocty == NULL) {
    printf("Allocation Error!!");
    return ;
  }
    
   i=0;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      k=bi*bx+bj;
      ii=b_pocty[k];
      cache_read(b_pocty+k);
      cache_read(zac_pocty+k);
      zac_pocty[k]=i;
      i+=ii;
    }
  }
  for(bi=0;bi<by;bi++)
  {
  // pres bloky v y
  //lj=adr[hr_y[bi]];

  prvni=1;
  for(bj=0;bj<bx;bj++)
  {
    // pres bloky v y
    k=bi*bx+bj;
    cache_read(b_pocty+k);
    if (b_pocty[k]==0) continue;
    cache_read(zac_pocty+k);
    t=zac_pocty[k];
    cache_read(hr_y+bi);
    cache_read(hr_y+bi+1);
    for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {

    i2=i-hr_y[bi];
    // pres radky    
    //uj = adr[i + 1];
    s=0.0;
    //cache_read(&(adrb[k][i2]));
    //cache_read(&(adrb[k][i2+1]));
    for (j = adrb[k][i2]; j < adrb[k][i2 + 1]; j++) {
    // v radku
      cache_read(cib + j +t);
      ii = cib[j+t];
      cache_read(pole_ab + j + t);
      cache_read(x + ii);
      s += pole_ab[j+t] * x[ii];
      //s += a[j] * x[ii];
    }
    cache_read(y + i);
    if (prvni) { y[i] = s; }
    else { y[i] += s; }
    //lj = uj;
    }
    prvni=0;
   // lj = uj;
   // cache_read(y + i);
  }
  }
} 


void mv_block16(float *x, float * rest y, uint n)
{
  // multiplication of 16bit*16bit block
  uint i,j,k;
  double start,end;

          #ifdef TIMER2
          start=omp_get_wtime();
          //std::cout << " CSR " << end-start << std::endl;
          #endif
            
  for(i=0;i<n;i++) y[i]=0.0;
  for(i=0;i<no_blocks;i++)
      {
        /*std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        fflush(stdout);    */
        //if (pole_pol[i].pocty>(1<<16))
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif
          mv_csr_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);          
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif
          mv_coo_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }
        /*std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */
      }
          #ifdef TIMER2
          end=omp_get_wtime();
          std::cout << " SPMV " << end-start << std::endl;        
          #endif    
   return;
}


void mv_plan(float *x, float * rest y, uint n)
{
  // for execution of plan
  // TODO 
  double start,start2,end,end2;
  uint podil=(n)/threads;//(n+threads-1)/threads;

          //std::cout << " CSR " << end-start << std::endl;
  omp_lock_t mutexy[MAX_BLO];

          start=omp_get_wtime();
  
  for(uint l=0;l<noboundsy;l++) omp_init_lock(mutexy+l);
          
  #pragma omp parallel num_threads(threads) private(end2) shared(start,start2)         
  {
    uint tid,j,k;

    tid=omp_get_thread_num();
    memset(y+tid*podil,0,podil*4);
     //for(uint i=0;i<n;i++) y[i]=0.0;
    //memset(y,0,n*4);
    #pragma omp master
       start2=omp_get_wtime();
  
    //#pragma omp for schedule(dynamic,1)
    for(uint i=0;i<no_blocks;i++)
    {
        if (pole_exec[i].done!=tid) continue;      
        /*std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        fflush(stdout);    */
        //if (pole_pol[i].pocty>(1<<16))
        k=0;
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          omp_set_lock(mutexy+j);          
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif
          mv_csr_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);          
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif
          mv_coo_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }
        /*std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */
          for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          omp_unset_lock(mutexy+j);  
  } 
          #ifdef TIMER3
          end2=omp_get_wtime();
          std::cout << " SPMV plan " << tid << " = "<< end2-start2 << std::endl;        
          #endif   
  }
          #ifdef TIMER2
          end=omp_get_wtime();
          std::cout << " SPMV plan " << end-start << std::endl;        
          #endif    
   return;
}


void mv_dyn(float *x, float * rest y, uint n)
{
  // for "no planning" execution
  uint l,executed=0;
  double start,start2,end,end2;
  uint podil=(n)/threads;//(n+threads-1)/threads;

          //std::cout << " CSR " << end-start << std::endl;
  omp_lock_t mutexy[MAX_BLO];

          start=omp_get_wtime();
   for(l=0;l<no_blocks;l++) {   
   pole_exec[l].done=0;
   //mx[i]=my[i]=0;
   }  
  for(l=0;l<noboundsy;l++) omp_init_lock(mutexy+l);
  int podil2=no_blocks/threads;        
  #pragma omp parallel num_threads(threads) private(end2) shared(start,start2)         
  {
    uint i,tid,j,k;

    tid=omp_get_thread_num();
    memset(y+tid*podil,0,podil*4);
     //for(uint i=0;i<n;i++) y[i]=0.0;
    //memset(y,0,n*4);
    #pragma omp master
       start2=omp_get_wtime();
  
    //#pragma omp for schedule(dynamic,1)
    i=tid*podil2-1;
    do//for(int i2=0;i2<no_blocks;i2++)
    {
          i++;
          if (i>=no_blocks) i-=no_blocks; 

        //i=(i2+tid*podil2)%no_blocks;
        /*std::cout << " tid= " << tid;
        std::cout << " i= " << i << std::endl;
        fflush(stdout); */
        #pragma omp flush(pole_exec)
        
        if (pole_exec[i].done>0) continue;      
        /*std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        fflush(stdout);    */
        //if (pole_pol[i].pocty>(1<<16))
        k=0;
        //do{
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          if (omp_test_lock(mutexy+j)) k++;
          else break;
        if (k!=(pole_pol[i].my2-pole_pol[i].my1)) 
        {  
          /*std::cout << " tid= " << tid;
          std::cout << " i2= " << i << std::endl;
          fflush(stdout);            
          */
          for(j=pole_pol[i].my1;j<(pole_pol[i].my1+k);j++)
            omp_unset_lock(mutexy+j);
          /*std::cout << " tid= " << tid;
          std::cout << " i3= " << i << std::endl;
          fflush(stdout);   */   
          #pragma omp flush(executed)                
          continue;  
        }   
        pole_exec[i].done=1;
        
        #pragma omp flush(pole_exec)
        
        /*std::cout << " tid= " << tid;
        std::cout << " i4= " << i << std::endl;
        fflush(stdout);   */
        
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif
          mv_csr_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);          
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif
          mv_coo_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }
        /*std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          omp_unset_lock(mutexy+j);  
        
        #pragma omp atomic 
          executed++;
        #pragma omp flush(executed)
  }while(executed<no_blocks); 
          #ifdef TIMER3
          end2=omp_get_wtime();
          std::cout << " SPMV " << tid << " = "<< end2-start2 << std::endl;        
          #endif   
  }
          #ifdef TIMER2
          end=omp_get_wtime();
          std::cout << " SPMV dyn " << end-start << std::endl;        
          #endif    
          std::cout << " SPMV exec" << executed << std::endl;   
   return;
}



void mv_many(float *x, float * rest y, uint n)
{
  // by reduction to final vector
  uint l,executed=0;
  double start,start2,end,end2;
  uint podil=(n)/threads;//(n+threads-1)/threads;

          //std::cout << " CSR " << end-start << std::endl;
  float *vec_y=alokacef((threads-1)*n); 
  //for(l=0;l<noboundsy;l++) omp_init_lock(mutexy+l);
  //int podil2=no_blocks/threads;        
  #pragma omp parallel num_threads(threads) private(end2) shared(start,start2)         
  {
    
    uint i,tid,j,k;
    float s;

    tid=omp_get_thread_num();
    if (tid==0)
      memset(y,0,n*4);
    else  
      memset(vec_y+(tid-1)*n,0,n*4);
     //for(uint i=0;i<n;i++) y[i]=0.0;
    //memset(y,0,n*4);
    #pragma omp master
       start2=omp_get_wtime();
  
    //#pragma omp for schedule(dynamic,1)
    #pragma omp for schedule(dynamic,1)        
    for(int i=0;i<no_blocks;i++)
    {
        
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif
          if (tid==0)
            mv_csr_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);          
          else 
            mv_csr_f16A(pole_pol[i].a, x+pole_pol[i].startx, vec_y+(tid-1)*n+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);          

          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif
          if (tid==0)          
            mv_coo_f16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          else 
            mv_coo_f16A(pole_pol[i].a, x+pole_pol[i].startx, vec_y+(tid-1)*n+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }
        /*std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */

  } // end of for
  #pragma omp for schedule(static)        
  for(int i=0;i<n;i++)
    {
      s=0.0;
      for(j=1;j<threads;j++)
        s+=vec_y[i+(tid-1)*n];
      y[i]+=s;  
    }
    
          #ifdef TIMER3
          end2=omp_get_wtime();
          std::cout << " SPMV " << tid << " = "<< end2-start2 << std::endl;        
          #endif      
  } // end of parallel  
  
 
    
          #ifdef TIMER2
          end=omp_get_wtime();
          std::cout << " SPMV many " << end-start << std::endl;        
          #endif    
   return;
}


void fused_block16(float *x, float *rest y, float *rest y2, uint n)
{
  // multiply of 16bit*16bit block
  uint i,j,kx,ky;
  double start,end;


          #ifdef TIMER2
          start=omp_get_wtime();
          //std::cout << " CSR " << end-start << std::endl;
          #endif
  
  for(i=0;i<n;i++) y[i]=y2[i]=0.0;
    
  for(i=0;i<no_blocks;i++)
      {
        std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        fflush(stdout);  
        //if (pole_pol[i].pocty>(1<<16))

            
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif

          fused_csr_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, x+pole_pol[i].starty);

            
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif

          fused_coo_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty, x+pole_pol[i].starty); 
          //fused_coo_f16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }
        /* std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */
      }
      
 #ifdef TIMER2
 end=omp_get_wtime();
 std::cout << " FUSSED " << end-start << std::endl;        
 #endif       
 return;
}


void fused_plan(float *x, float *rest y, float *rest y2, uint n)
{
  // fused mult. by plan 
  // TODO predelat

  uint i,j,kx,ky;
  double start,start2,end,end2;

  omp_lock_t mutexx[MAX_BLO];
  omp_lock_t mutexy[MAX_BLO];
  
          start=omp_get_wtime();

  for(uint l=0;l<noboundsx;l++) omp_init_lock(mutexx+l);
  
  for(uint l=0;l<noboundsy;l++) omp_init_lock(mutexy+l);
  
  #pragma omp parallel for          
  for(uint i=0;i<n;i++) y[i]=y2[i]=0.0;
  
  start2=omp_get_wtime();
  
  #pragma omp parallel shared(start,start2) num_threads(threads) private(end2)
  {
  uint tid,j,k;
  tid=omp_get_thread_num();
  for(uint i=0;i<no_blocks;i++)
  {
        if (pole_exec[i].done!=tid) continue;   
        /*std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        fflush(stdout);  */
        //if (pole_pol[i].pocty>(1<<16))
        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++)
          omp_set_lock(mutexx+j);
         for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          omp_set_lock(mutexy+j);
          
            
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          #ifdef TIMER1
          start=omp_get_wtime();
          #endif

          fused_csr_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey, x+pole_pol[i].starty);

            
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " CSR " << end-start << std::endl;
          #endif
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          #ifdef TIMER1
          start=omp_get_wtime();          
          #endif

          fused_coo_f16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty, x+pole_pol[i].starty); 
          //fused_coo_f16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          #ifdef TIMER1
          end=omp_get_wtime();
          std::cout << " COO " << end-start << std::endl;        
          #endif
        }
        /* std::cout << "Block no. " << i << " finished." << std::endl;
        fflush(stdout);
        */
        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++)
          omp_unset_lock(mutexx+j);
         for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
          omp_unset_lock(mutexy+j);

      } 
          #ifdef TIMER3
          end2=omp_get_wtime();
          std::cout << " FUSSED " << tid << " = "<< end2-start2 << std::endl;        
          #endif         
      
      }
      
 #ifdef TIMER2
 end=omp_get_wtime();
 std::cout << " FUSSED " << end-start << std::endl;        
 #endif       
 return;
}

#undef GENERATOR
#undef BACKWARDS
#include "fused_dyn.cpp"
#define BACKWARDS
#include "fused_dyn.cpp"
#undef BACKWARDS
#define GENERATOR
#include "fused_dyn.cpp"
 

#undef GENERATOR
#undef BACKWARDS
#include "fused_single.cpp"
#define BACKWARDS
#include "fused_single.cpp"
#undef BACKWARDS
#define GENERATOR
#include "fused_single.cpp"


#undef GENERATOR
#undef BACKWARDS
#include "fused_single2.cpp"
#define BACKWARDS
#include "fused_single2.cpp"
#undef BACKWARDS
#define GENERATOR
#include "fused_single2.cpp"


#undef BACKWARDS 
#include "fused_dyn_plan.cpp"
#define BACKWARDS 1
#include "fused_dyn_plan.cpp"





void mv_block_cache16(float *x, float *y, uint n)
{
  // cache
  uint i,j,k;
  
  char prvnix[MAX_BLO],prvniy[MAX_BLO];
  
  for(i=0;i<noboundsx;i++){ prvnix[i]=0;  cache_read(prvnix+i); }
  for(i=0;i<noboundsy;i++){ prvniy[i]=0;  cache_read(prvniy+i); }
  
  for(i=0;i<no_blocks;i++)
      {
        /*std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl; */
        //if (pole_pol[i].pocty>(1<<16))
        k=0;
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
        {  
          k+=prvniy[j]; 
          cache_read(prvniy+j);
        }        
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          if (k==0)
          {  
            mv_csr_f_cache16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);
            for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
            {
              prvniy[j]=1;
              cache_read(prvniy+j);
            }  
          }  
          else
            mv_csr_f_cache16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);          
          //mv_csr_f_cache16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 
          if (k==0)
          {  
            mv_coo_f_cache16S(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
            for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)             
            {
              prvniy[j]=1;
              cache_read(prvniy+j);
            }
          }  
          else
            mv_coo_f_cache16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
          
          //mv_coo_f16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
        }
      }
   return;
}


void fused_block_cache16(float *x, float *y, float *y2, uint n)
{
  // cache
 uint i,j,kx,ky;
  double start,end;
  char prvnix[MAX_BLO],prvniy[MAX_BLO];
  
  for(i=0;i<noboundsx;i++) prvnix[i]=0;
  for(i=0;i<noboundsy;i++) prvniy[i]=0;
    
  for(i=0;i<no_blocks;i++)
      {
        /*std::cout << "Block no. " << i;
        std::cout << " xs= " << pole_pol[i].startx;
        std::cout << " ys= " << pole_pol[i].starty;
        std::cout << " sx= " << pole_pol[i].sizex;
        std::cout << " sy= " << pole_pol[i].sizey;
        //std::cout << "Block no. " << pole_pol[no_blocks].x=cib+zac_pocty[i];
        //pole_pol[no_blocks].a=pole_ab+zac_pocty[i];
        //pole_pol[no_blocks].temp=adrb[i];
        std::cout << " p= " << pole_pol[i].pocty << std::endl;
        fflush(stdout);  */
        //if (pole_pol[i].pocty>(1<<16))
        kx=ky=0;
        for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++)
            kx+=prvnix[j];
        for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++)
            ky+=prvniy[j];
            
        if (pole_pol[i].pocty>pole_pol[i].sizey)
        {
          //mv_csr_f_cache16(float *a, float *x, float *y, uint *adr, usint *ci, uint n) 
          if (ky==0)
            if (kx==0)
            {
              fused_csr_f_cache16S(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);
              for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++) prvnix[j]=1;
              for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++) prvniy[j]=1;
            }
            else
            {
              fused_csr_f_cache16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);
              //for(j=pole_pol[i].mx1;j<pole_pol[i].mx2) prvnix[j]=1;
              for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++) prvniy[j]=1;
            }
          else
            if (kx==0)
            {
              fused_csr_f_cache16S(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);
              for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++) prvnix[j]=1;
              //for(j=pole_pol[i].my1;j<pole_pol[i].my2) prvniy[j]=1;
            }
            else
            {
              fused_csr_f_cache16A(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, (uint *)pole_pol[i].temp, pole_pol[i].x, pole_pol[i].sizey);
              //for(j=pole_pol[i].mx1;j<pole_pol[i].mx2) prvnix[j]=1;
              //for(j=pole_pol[i].my1;j<pole_pol[i].my2) prvniy[j]=1;
            }
            
        }
        else
        {
          //mv_coo_f_cache16(float *a, float *x, float *y, usint *x_pos, usint *y_pos,uint n, uint nnz) 

          if (ky==0)
            if (kx==0)
            {
              fused_coo_f_cache16SS(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty);
              for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++) prvnix[j]=1;
              for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++) prvniy[j]=1;
            }
            else
            {
              fused_coo_f_cache16SA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
              //for(j=pole_pol[i].mx1;j<pole_pol[i].mx2) prvnix[j]=1;
              for(j=pole_pol[i].my1;j<pole_pol[i].my2;j++) prvniy[j]=1;
            }
          else
            if (kx==0)
            {
              fused_coo_f_cache16AS(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
              for(j=pole_pol[i].mx1;j<pole_pol[i].mx2;j++) prvnix[j]=1;
              //for(j=pole_pol[i].my1;j<pole_pol[i].my2) prvniy[j]=1;
            }
            else
            {
              fused_coo_f_cache16AA(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 
              //for(j=pole_pol[i].mx1;j<pole_pol[i].mx2) prvnix[j]=1;
              //for(j=pole_pol[i].my1;j<pole_pol[i].my2) prvniy[j]=1;
            }          
          //fused_coo_f16(pole_pol[i].a, x+pole_pol[i].startx, y+pole_pol[i].starty, y2+pole_pol[i].startx, pole_pol[i].x, (usint *)pole_pol[i].temp,pole_pol[i].sizey, pole_pol[i].pocty); 

        }
        //std::cout << "Block no. " << i << " finished." << std::endl;
        //fflush(stdout);

      }
   return;
}


void fused_block_cache(float *x, float *y, float *y2, ullong n,uint *hr_x,uint bx,uint *hr_y,uint by,ullong *b_pocty,float *pole_ab,ullong **adrb, uint *cib) {
  // input n
  //       hranice:
  //         hr_x,bx,hr_y,by
  //       pocty:
  //         b_pocty
  // output: block CSR
  //         pole_ab,adrb, cib
  ullong bi,bj,i,i2, j, ii, lj, uj,t;
  uint k,prvni;
  //int first[MAX_BLO];
  ullong old[MAX_BLO],*ladrb[MAX_BLO];
  ullong *zac_pocty = (ullong *)malloc(bx * by * sizeof(ullong));
  float s,xi;
  
  if (zac_pocty == NULL) {
    printf("Chyba alokace!!");
    return ;
  }
  /*
    for(bj=0;bj<bx;bj++)
    {
      cache_read(first+bj);
      first[bj]=1;
    } */  
   i=0;
  for(bi=0;bi<by;bi++)
  {
    for(bj=0;bj<bx;bj++)
    {
      k=bi*bx+bj;
      ii=b_pocty[k];
      cache_read(b_pocty+k);
      cache_read(zac_pocty+k);
      zac_pocty[k]=i;
      i+=ii;
    }
  }
  for(bi=0;bi<by;bi++)
  {
  // pres bloky v y
  //lj=adr[hr_y[bi]];

  prvni=1;
  for(bj=0;bj<bx;bj++)
  {
    // pres bloky v y
    k=bi*bx+bj;
    cache_read(b_pocty+k);
    if (bi==0)
    {
      for(i=hr_x[bj];i<hr_x[bj+1];i++)
      {
        y2[i]=0.0;
        cache_read(y2+i);
      }
    }
    if (b_pocty[k]==0) continue;
    cache_read(zac_pocty+k);
    t=zac_pocty[k];
    cache_read(hr_y+bi);
    cache_read(hr_y+bi+1);
    for (i = hr_y[bi]; i < hr_y[bi+1]; i++) {
    cache_read(x+i);
    xi=x[i];
    i2=i-hr_y[bi];
    // pres radky    
    //uj = adr[i + 1];
    s=0.0;
    //cache_read(&(adrb[k][i2]));
    //cache_read(&(adrb[k][i2+1]));
    for (j = adrb[k][i2]; j < adrb[k][i2 + 1]; j++) {
    // v radku
      cache_read(cib + j +t);
      ii = cib[j+t];
      cache_read(pole_ab + j + t);
      cache_read(x + ii);
      s += pole_ab[j+t] * x[ii];
      cache_read(y2 + ii);
      y2[ii]+=  pole_ab[j+t]*xi; 
      //s += a[j] * x[ii];
    }
    cache_read(y + i);
    if (prvni) { y[i] = s; }
    else { y[i] += s; }
    //lj = uj;
    }
    prvni=0;
   // lj = uj;
   // cache_read(y + i);
  }
  }
} 
